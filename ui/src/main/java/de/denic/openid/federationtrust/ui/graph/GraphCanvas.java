/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.ui.graph;

import org.jgrapht.graph.*;
import org.jgrapht.ext.*;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxICell;
import com.mxgraph.util.mxConstants;

import de.denic.openid.federationtrust.business.EntityVertex;
import de.denic.openid.federationtrust.business.FedEntityStmtEdge;
import de.denic.openid.federationtrust.business.TrustGraph;
import de.denic.openid.federationtrust.ui.common.Algebra;
import de.denic.openid.federationtrust.ui.common.Size;
import de.denic.openid.federationtrust.ui.event.GraphCanvasClickEvent;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.micronaut.core.annotation.NonNull;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.geom.Point2D;

@Tag("canvas")
public class GraphCanvas extends Component implements HasStyle, HasSize {
    
    private static final long serialVersionUID = 4239908754285292871L;

    private static final Color AUTO_DEFAULT_STROKE_COLOR = new Color(0,0,0,255);
    private static final Color AUTO_TRUST_STROKE_COLOR = new Color(0,255,0,255);
    private static final Color AUTO_UNTRUST_STROKE_COLOR = new Color(255,0,0,255);
    private static final Color AUTO_LEAF_STROKE_COLOR = new Color(0,0,255,255);
    private static final Color VERTEX_DEFAULT_FILL_COLOR = new Color(255,255,255,255);
    private static final Color VERTEX_DEFAULT_FILL_HIGHLIGHT_COLOR = new Color(181,215,255,255);
    private static final Font  VERTEX_DEFAULT_FONTSTYLE = new Font(Font.SANS_SERIF,Font.PLAIN,15);
    private static final VERTEX_SHAPE VERTEX_DEFAULT_SHAPE = VERTEX_SHAPE.OVAL;
    private static final int   VERTEX_DEFAULT_LINE_WIDTH = 2;
    private static final int   EDGE_DEFAULT_LINE_WIDTH = 1;
    private static final int   EDGE_DEFAULT_ARROW_SIZE = 6;

    
    private GraphCanvasRenderingContext2D context;

    private final TrustGraph trustGraph;
    private final AsUnmodifiableGraph<EntityVertex, FedEntityStmtEdge> graph;
    private JGraphXAdapter<EntityVertex, FedEntityStmtEdge> jgxAdapter;

    private EntityVertex selectedVertex;
    private final Size size;
    private  int offSetX;
    private  int offSetY;
    private final HashMap<EntityVertex, mxICell> gridLookup;

    public GraphCanvas(@NonNull Size size, final int paddingX, final int paddingY, final int multiplicator,
                      @NonNull final  TrustGraph trustGraph, @NonNull final  ComponentEventListener<GraphCanvasClickEvent> clickListener) {

        context = new GraphCanvasRenderingContext2D(this);
        getElement().setAttribute("width", String.valueOf(size.getWidth()));
        getElement().setAttribute("height", String.valueOf(size.getHeight()));

        this.size = requireNonNull(size, "Missing Size Object");
        this.trustGraph = requireNonNull(trustGraph, "Missing Trustgraph component");
        this.graph = requireNonNull(trustGraph.getGraph(), "Missing JGraphT component");
        this.jgxAdapter =  new JGraphXAdapter<>(graph);
        requireNonNull(clickListener, "Missing GraphCanvasClickEvent Listener component");
        gridLookup = jgxAdapter.getVertexToCellMap();
        selectedVertex = null;
        addListener(GraphCanvasClickEvent.class, clickListener);
        initGraphLayout();
        drawGraph();

        setWidth(this.size.getWidthSize());
        setHeight(this.size.getHeightSize());
        setMinWidth(this.size.getWidthSize());
        setMinHeight(this.size.getHeightSize());
        setMaxWidth(this.size.getWidthSize());
        setMaxHeight(this.size.getHeightSize());
    }

    private void initGraphLayout(){

        ArrayList<mxICell> cells = new ArrayList<mxICell>();

        jgxAdapter.getModel().beginUpdate();

        jgxAdapter.getStylesheet().getDefaultVertexStyle().put(mxConstants.STYLE_FONTSIZE, VERTEX_DEFAULT_FONTSTYLE.getSize()+3);
        jgxAdapter.getStylesheet().getDefaultVertexStyle().put(mxConstants.STYLE_FONTFAMILY, VERTEX_DEFAULT_FONTSTYLE.getName());
        jgxAdapter.getStylesheet().getDefaultVertexStyle().put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_ELLIPSE);

        gridLookup.forEach( (vertex, cell) -> {
            String[] vLabelSplit = vertex.getEntity().getPlainValue().split("/");
            cell.setValue(".../"+vLabelSplit[vLabelSplit.length-1]);
            //cell.setStyle(";shape=ellipse;fontfamily=SansSerif;fontSize=30");
            //jgxAdapter.getModel().setStyle(cell, ";shape=ellipse;fontfamily=SansSerif;fontSize=20");
            //jgxAdapter.setCellStyle(";fontSize=30", new mxICell[]{cell});
            jgxAdapter.updateCellSize(cell);
            cells.add(cell);
                                    
        });

        jgxAdapter.getModel().endUpdate();
        jgxAdapter.refresh();

        //mxCompactTreeLayout layout = new mxCompactTreeLayout(jgxAdapter, false);
        //layout.execute(jgxAdapter.getDefaultParent());

       // mxParallelEdgeLayout layout = new mxParallelEdgeLayout(jgxAdapter);
       // layout.execute(jgxAdapter.getDefaultParent());

        mxHierarchicalLayout layout = new mxHierarchicalLayout(jgxAdapter);
        layout.execute(jgxAdapter.getDefaultParent());

        Rectangle bound = jgxAdapter.getBoundsForCells(cells.toArray(), true, false, false).getRectangle();
        offSetX = (int) Math.abs( (size.getWidth() - bound.getWidth()) /2 );
        offSetY = (int) Math.abs( (size.getHeight() - bound.getHeight()) /2);

        jgxAdapter.getModel().beginUpdate();
        gridLookup.forEach( (vertex, cell) -> {
            cell.getGeometry().translate(offSetX, offSetY);
        });
        jgxAdapter.getModel().endUpdate();
        jgxAdapter.refresh();
    }

    public void drawGraph() {
        // draw edges of graph
        for (FedEntityStmtEdge edge : graph.edgeSet()) {
            graph.getEdgeSource(edge);
            drawEdge(graph.getEdgeSource(edge), graph.getEdgeTarget(edge), getAutoEdgeColor(edge), EDGE_DEFAULT_LINE_WIDTH, EDGE_DEFAULT_ARROW_SIZE);
        }
        
        // draw vertices of graph
        for (EntityVertex vertex : graph.vertexSet()) {
            if ((selectedVertex != null) && (vertex.equals(selectedVertex))) {
                drawVertex(vertex, getAutoVertexColor(vertex), VERTEX_DEFAULT_FILL_HIGHLIGHT_COLOR,
                            VERTEX_DEFAULT_FONTSTYLE, VERTEX_DEFAULT_SHAPE, VERTEX_DEFAULT_LINE_WIDTH);
            } else {
                drawVertex(vertex, getAutoVertexColor(vertex), VERTEX_DEFAULT_FILL_COLOR,
                            VERTEX_DEFAULT_FONTSTYLE, VERTEX_DEFAULT_SHAPE, VERTEX_DEFAULT_LINE_WIDTH);
            }
        }
    }

    public void drawVertex(@NonNull final EntityVertex vertex, @NonNull final Color strokeColor,
                             @NonNull final Color fillColor, @NonNull final Font font, @NonNull final VERTEX_SHAPE shape, final int lineWidth) {

        final GraphCanvasRenderingContext2D crx = getContext();

        final mxICell cell = gridLookup.get(vertex);
        mxGeometry geometry = cell.getGeometry();

        int srcX = (int) geometry.getCenterX();
        int srcY = (int) geometry.getCenterY();
        int width = (int) geometry.getWidth();
        int height = (int) geometry.getHeight();
        int srcXLabel = (int) (geometry.getX() + (width * 0.15)/2);
        int srcYLabel = (int) (geometry.getY()+ height - (height * 0.5)/2);

        String vertexLabel = cell.getValue().toString();

        switch(shape) {
            case CIRC:
                int r = (int) ((width > height)? height/2: width/2);
                crx.save();
                crx.setLineWidth(lineWidth);
                crx.setStrokeStyle(AWTColorToJsString(strokeColor));
                crx.setFillStyle(AWTColorToJsString(fillColor));
                crx.beginPath();
                crx.arc(srcX, srcY, r, 0, 2 * Math.PI, false);
                crx.closePath();
                crx.stroke();
                crx.fill();
                crx.setFillStyle("rgba(0,0,0,255)");
                crx.setFont(AWTFontToJsString(font));
                crx.fillText(vertexLabel, geometry.getX(), srcY);
                crx.restore();
                break;
            case OVAL:
                crx.save();
                crx.setLineWidth(lineWidth);
                crx.setStrokeStyle(AWTColorToJsString(strokeColor));
                crx.setFillStyle(AWTColorToJsString(fillColor));
                crx.beginPath();
                crx.ellipse(srcX, srcY, width/2, height/2, 0, 0, 2*Math.PI, false);
                crx.stroke();
                crx.fill();
                crx.closePath();
                crx.restore();
                crx.save();
                crx.setFillStyle("rgba(0,0,0,255)");
                crx.setFont(AWTFontToJsString(font));
                crx.fillText(vertexLabel, srcXLabel, srcYLabel);
                crx.restore();
                break;
            case RECT:
                crx.save();
                crx.setLineWidth(lineWidth);
                crx.setStrokeStyle(AWTColorToJsString(strokeColor));
                crx.setFillStyle(AWTColorToJsString(fillColor));
                crx.strokeRect(geometry.getX(), geometry.getY(), width, height);
                crx.fillRect(geometry.getX(), geometry.getY(), width, height);
                crx.setFillStyle("rgba(0,0,0,255)");
                crx.setFont(AWTFontToJsString(font));
                crx.fillText(vertexLabel, srcXLabel, srcYLabel);
                crx.restore();
                break;
        }
    }

    public void reDrawVertex(EntityVertex vertex, Color color, Font font, VERTEX_SHAPE shape, int lineWidth) {
        //final CanvasRenderingContext2D crx = getContext();
        drawVertex(vertex, getAutoVertexColor(vertex), color, font, shape, lineWidth);
    }

    public void drawEdge(@NonNull final EntityVertex srcVertex, @NonNull final EntityVertex destVertex,
                            @NonNull final Color color, final int lineWidth, final int arrowSize) {

        final GraphCanvasRenderingContext2D crx = getContext();
        
        final mxICell srcCell = gridLookup.get(srcVertex);
        final mxICell destCell = gridLookup.get(destVertex);
       
        int srcX = (int) srcCell.getGeometry().getCenterX();
        int srcY = (int) srcCell.getGeometry().getCenterY();
        int destX = (int) destCell.getGeometry().getCenterX();
        int destY = (int) destCell.getGeometry().getCenterY();
        int width = (int) destCell.getGeometry().getWidth();
        int height = (int) destCell.getGeometry().getHeight();

        Point2D intersect = Algebra.getIntersection(srcX, destX, srcY, destY, destX, destY, width/2.0, height/2.0).get(0);
      
        destX = (int) intersect.getX();
        destY = (int) intersect.getY();
        double x=0, y=0, angle=0;

        // adapt edge length towards arrow peak distance 
        angle = Math.atan2(destY - srcY, destX - srcX);
        x = destX+ arrowSize * Math.cos(angle);
        y = destY+ arrowSize * Math.sin(angle);
        int deltaX = (int) (destX - x);
        int deltaY = (int) (destY - y);
        destX += deltaX;// + deltaX*(deltaX/deltaY);
        destY += deltaY;// + deltaY*(deltaX/deltaY);

        crx.save();
        crx.setLineWidth(lineWidth);
        crx.setStrokeStyle(AWTColorToJsString(color));
        crx.beginPath();
        crx.moveTo(srcX, srcY);
        crx.lineTo(destX, destY);
        crx.closePath();
        crx.stroke();
        crx.restore();

        crx.save();
        crx.setLineWidth(arrowSize);
        crx.setFillStyle(AWTColorToJsString(color));
        crx.beginPath();

        angle = Math.atan2(destY - srcY, destX - srcX);
        x = destX+ arrowSize * Math.cos(angle);
        y = destY+ arrowSize * Math.sin(angle);
        crx.moveTo(x, y);

        angle += (1.0/3.0) * (2 * Math.PI);
        x = destX + arrowSize * Math.cos(angle);
        y = destY + arrowSize * Math.sin(angle);
        crx.lineTo(x, y);

        angle += (1.0/3.0) * (2 * Math.PI);
        x = destX + arrowSize *Math.cos(angle);
        y = destY + arrowSize *Math.sin(angle);
        crx.lineTo(x, y);
        
        crx.closePath();
        //crx.stroke();
        crx.fill();
        crx.restore();
    }

    private Color getAutoEdgeColor(FedEntityStmtEdge edge) {
        // check if edge target is valid
        if (graph.getEdgeTarget(edge).isValid()) {
            return AUTO_TRUST_STROKE_COLOR;
        } else {
           return AUTO_UNTRUST_STROKE_COLOR;
        }
    }

    private Color getAutoVertexColor(EntityVertex vertex) {
        // check for leave node
        if (trustGraph.getLeafVertex().equals(vertex)) {
            return AUTO_LEAF_STROKE_COLOR;
        }
        // check  vertex validity
        else if (vertex.isValid()) {
            // check if intermediate vertex
            List<EntityVertex> childs = getAllChildsOfVertex(vertex);
            if (childs.size() == 0) {
                // trust anchor color
                return AUTO_TRUST_STROKE_COLOR;
            }
        // invalid vertex
        } else {
           return AUTO_UNTRUST_STROKE_COLOR;
        }
        return AUTO_DEFAULT_STROKE_COLOR;
    }

    protected String AWTColorToJsString(@NonNull final Color color) {
        return String.format("rgba(%s, %s, %s, %s)", color.getRed(),
                             color.getGreen(), color.getBlue(), color.getAlpha());
    }

    protected String AWTFontToJsString(@NonNull final Font font){
        return String.format("%spx %s", font.getSize(), font.getName());
    }

    protected enum VERTEX_SHAPE {
        RECT,
        CIRC,
        OVAL
      }

    public EntityVertex getVertexAtCoordinate(final int xCoord, final int yCoord) {

        EntityVertex vertex = null;

        for (Map.Entry<EntityVertex, mxICell> entry : gridLookup.entrySet()) {
            Rectangle rect = entry.getValue().getGeometry().getRectangle();
                if (rect.contains(xCoord, yCoord)) {
                    vertex = entry.getKey();
                }
        }
        highlightSelectedVertex(vertex);
        return vertex;
    }

    private void clearCanvas() {
        final GraphCanvasRenderingContext2D crx = getContext();
        crx.save();
        crx.clearRect(0, 0, this.size.getWidth(), this.size.getHeight());
        crx.restore();
    }

    private void highlightSelectedVertex(final EntityVertex vertex) {
        selectedVertex = vertex;
        clearCanvas();
        drawGraph();
    }

    private List<EntityVertex> getAllChildsOfVertex(EntityVertex vertex) {

        ArrayList<EntityVertex> childs = new ArrayList<EntityVertex>();

        for (FedEntityStmtEdge edge : graph.outgoingEdgesOf(vertex)) {
            childs.add(graph.getEdgeTarget(edge));
        }
        return childs;
    }

    public GraphCanvasRenderingContext2D getContext() {
        return context;
    }

     /**
     * {@inheritDoc}
     * <p>
     * <b>NOTE:</b> Canvas has an internal coordinate system that it uses for
     * drawing, and it uses the width and height provided in the constructor.
     * This coordinate system is independent of the component's size. Changing
     * the component's size with this method may scale/stretch the rendered
     * graphics.
     */
    @Override
    public void setWidth(String width) {
        HasSize.super.setWidth(width);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <b>NOTE:</b> Canvas has an internal coordinate system that it uses for
     * drawing, and it uses the width and height provided in the constructor.
     * This coordinate system is independent of the component's size. Changing
     * the component's size with this method may scale/stretch the rendered
     * graphics.
     */
    @Override
    public void setHeight(String height) {
        HasSize.super.setHeight(height);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <b>NOTE:</b> Canvas has an internal coordinate system that it uses for
     * drawing, and it uses the width and height provided in the constructor.
     * This coordinate system is independent of the component's size. Changing
     * the component's size with this method may scale/stretch the rendered
     * graphics.
     */
    @Override
    public void setSizeFull() {
        HasSize.super.setSizeFull();
    }
}
