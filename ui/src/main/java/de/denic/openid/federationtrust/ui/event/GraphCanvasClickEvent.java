/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.ui.event;

import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;

import de.denic.openid.federationtrust.ui.graph.GraphCanvas;

@DomEvent("click")
public class GraphCanvasClickEvent extends UIClickEvent<GraphCanvas> {

    private static final long serialVersionUID = -8232451832178835039L;

    public GraphCanvasClickEvent(GraphCanvas source,
            boolean fromClient,
            @EventData("event.offsetX") int offsetX,
            @EventData("event.offsetY") int offsetY,
            @EventData("event.screenX") int screenX,
            @EventData("event.screenY") int screenY,
            @EventData("event.clientX") int clientX,
            @EventData("event.clientY") int clientY,
            @EventData("event.detail") int clickCount,
            @EventData("event.button") int button,
            @EventData("event.ctrlKey") boolean ctrlKey,
            @EventData("event.shiftKey") boolean shiftKey,
            @EventData("event.altKey") boolean altKey,
            @EventData("event.metaKey") boolean metaKey) {
        super(source, fromClient, offsetX, offsetY, screenX, screenY, clientX, clientY, clickCount, button, ctrlKey, shiftKey, altKey, metaKey);
    }    
}