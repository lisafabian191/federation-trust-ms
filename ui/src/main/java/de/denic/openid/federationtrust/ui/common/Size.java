/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.ui.common;

import java.util.HashMap;
import java.util.Map;

import io.micronaut.core.annotation.NonNull;

public class Size {

    private final int width;
    private final int height;
    private final String unit;
    private final SIZE_UNIT unitType;

    public Size(final int width, final int height, final SIZE_UNIT unitType) {
        this.width = width;
        this.height = height;
        this.unitType = unitType;
      
        switch (unitType) {
            case EM:
                unit = SIZE_UNIT.EM.getUnit();
                break;
            case PCT:
                unit = SIZE_UNIT.PCT.getUnit();
                break;
            default:
                unit = SIZE_UNIT.PX.getUnit();
        }
    }

    public Size(@NonNull final String widthSize, @NonNull final String heightSize) {

        String regex = String.format("\\d+(%s|\\%s|%s)", SIZE_UNIT.EM.getUnit(), SIZE_UNIT.PCT.getUnit(), SIZE_UNIT.PX.getUnit());
        if ( widthSize.matches(regex) &&
             heightSize.matches(regex) ) {

           String widthSizeUnit = widthSize.split("\\d+")[1];
           String widthSizeRAW = widthSize.split(widthSizeUnit)[0];
           String heightSizeRAW = heightSize.split(widthSizeUnit)[0];
           width = Integer.parseInt(widthSizeRAW);
           height = Integer.parseInt(heightSizeRAW);
           unit = widthSizeUnit;
           unitType = SIZE_UNIT.get(widthSizeUnit);
         }
         else {
             width = 0; height = 0;
             unit = SIZE_UNIT.PX.getUnit();
             unitType = SIZE_UNIT.PX;
         }
    }

    public String getWidthSize() {
        return String.format("%s%s", width, unit);
    }

    public int getWidth() {
        return width;
    }

    public String getHeightSize() {
        return String.format("%s%s", height, unit);
    }

    public int getHeight() {
        return height;
    }

    public SIZE_UNIT getUnitType() {
        return unitType;
    }

    public String getUnit() {
        return unit;
    }

    public enum SIZE_UNIT {
        EM("em"),
        PCT("%"),
        PX("px");

        private final String unit;

        SIZE_UNIT(@NonNull final String unit) {this.unit = unit;};

        public String getUnit() {
            return unit;
        }

        //Lookup table
        private static final Map<String, SIZE_UNIT> lookup = new HashMap<>();
  
        //Populate the lookup table on loading time
        static
        {
            for(SIZE_UNIT unit : SIZE_UNIT.values())
            {
                lookup.put(unit.getUnit(), unit);
            }
        }
  
        //This method can be used for reverse lookup purpose
        public static SIZE_UNIT get(String unit) 
        {
            return lookup.get(unit);
        }
    }
}