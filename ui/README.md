# Federation Trust Web-UI

This project implements a simplified web-interface for federation trust verification. It uses vaadin 14 framework for server-side based ui rendering.


## Building

Project's artifacts are built with [Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) using the [vaadin gradle plugin](#ui-framework-vaadin-gradle-plugin).
You do **not** have to install this tool by hand, it bootstraps itself if necessary.

### Executable [`shadow` (or `fat`/`uber`)](https://github.com/johnrengelman/shadow) JAR file

To create an executable JAR file in developer mode issue:

```bash
$ ./gradlew clean build
```

To create an executable JAR file in production mode (e.g used for CI deployment) issue:


```bash
$ ./gradlew clean build -Pvaadin.productionMode
```

You will find the result in folder `./ui/build/libs`.

### [Docker](https://www.docker.com/) image

Project uses [Google's JIB Gradle plugin](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin) to
create Docker artifacts **without** requiring a local Docker daemon. That's why you won't find a
`Dockerfile` with the project.  

Creating a `tar` file containing the image, and upload the resulting artifact to your local image repository works like this:

```bash
$ ./gradlew jibBuildTar
[...]

$ docker load --input ui/build/jib-image.tar
[...]

$ docker image list | grep federation
registry.gitlab.com/id4me/federation-trust-ms/ui    latest    c6545cd7d7e0    5 seconds ago    121MB
```

More details on [JIB's documentation](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin).


## Running

General information: Application logs the HTTP endpoint it listens on to console:

```
[...]
INFO PID --- [main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
[...]
```

### Local Java Runtime Environment (JRE)

At least JRE version 11 is required!

```bash
$ java -jar ./ui/build/libs/federationtrust-ui-0.0.1.jar
```

### Docker container

Launching a container from the image built previously:

```bash
$ docker run --publish 8080:8080 registry.gitlab.com/id4me/federation-trust-ms/ui
[...]
```

Now you can reach the container from your host computer:

```bash
$ curl --silent localhost:8080/actuator/health | jq .
{
  "status": "UP"
}
```

### Local with [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

```bash
$ cd ui

/ui$ heroku local
[...]
```


## Deployment

### To [Heroku](https://www.heroku.com/) platform

**Prerequisite** is an account on Heroku platform.
[Login to your account](https://devcenter.heroku.com/articles/heroku-cli#getting-started) if not already done.

#### Via [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
  
First of all you need [Heroku's Java CLI plugin](https://github.com/heroku/plugin-java):
```bash
$ heroku plugins:install java
Installing plugin java... installed v3.1.1
```

Now create the Heroku app (which exists only local, not on Heroku's Git repo or something like that), here with the app name `federation-trust-ui` in Heroku's EU region, using project's Heroku [Procfile](ui/Procfile):
```bash
$ heroku create --no-remote --manifest --region eu federation-trust-ui
Creating ⬢ federation-trust-ui... done, region is eu
https://federation-trust-ui.herokuapp.com/ | https://git.heroku.com/federation-trust-ui.git
```

As you can see, application domain `federation-trust-ui.herokuapp.com` now is blocked for your attempts, so you have to choose another app name, sorry :blush:

Final step: Deployment of the app's artifact, here our executable JAR file:

```bash
$ cd ui

/ui$ heroku deploy:jar --app federation-trust-ui --jdk 14 build/libs/federationtrust-ui-0.0.1.jar
Uploading federationtrust-ui-0.0.1.jar
-----> Packaging application...
       - app: federation-trust-ui
       - including: build/libs/federationtrust-ui-0.0.1.jar
-----> Creating build...
       - file: slug.tgz
       - size: 50MB
-----> Uploading build...
       - success
-----> Deploying...
remote: 
remote: -----> heroku-deploy app detected
remote: -----> Installing JDK 14... done
remote: -----> Discovering process types
remote:        Procfile declares types -> web
remote: 
remote: -----> Compressing...
remote:        Done: 116.9M
remote: -----> Launching...
remote:        Released v8
remote:        https://federation-trust-ui.herokuapp.com/ deployed to Heroku
remote: 
-----> Done
```

Querying Heroku apps now gives:
```bash
$ heroku apps
=== [USER_ACCOUNT] Apps
federation-trust-ui (eu)
[...]
```

Getting more info about this app:
```bash
$ heroku apps:info --app federation-trust-ui
=== federation-trust-ui
Auto Cert Mgmt: false
Dynos:          web: 1
Git URL:        https://git.heroku.com/federation-trust-ui.git
Owner:          schoenbach@denic.de
Region:         eu
Repo Size:      0 B
Slug Size:      117 MB
Stack:          heroku-18
Web URL:        https://federation-trust-ui.herokuapp.com/
```

#### Via [Gradle's Heroku plugin](https://github.com/heroku/heroku-gradle)

Simply run:

```bash
$ ./gradlew deployHeroku
[...]
```

### With [helm (Kubernetes package manager)](https://helm.sh/)

This project offers a public helm chart repo for all released versions under the [projects gitlab webpage](https://id4me.gitlab.io/federation-trust-ms/index.yaml).

First of all you have add the chart repo to your local helm config (only once):
```bash
$ helm repo add id4me-charts https://id4me.gitlab.io/federation-trust-ms
"id4me-charts" has been added to your repositories
```

To get the latest updates and changes from the repo issue the following:
```bash
$ helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "id4me-charts" chart repository
Update Complete. ⎈ Happy Helming!⎈
```

Check the available versions with:
```bash
$ helm search repo id4me-charts/federation-trust-ui --versions
NAME                            	CHART VERSION 	APP VERSION   	DESCRIPTION                                       
id4me-charts/federation-trust-ui	1.0.1+ef5c3738	1.0.1-ef5c3738	A simplified ui for OpenID based federation tru...
```

In order to deploy a corresponding version (parameter &lt;CHART VERSION&gt;) directly from the repo into a K8s clusters namespace (parameter &lt;NAMESPACE&gt;) use the following command:
```bash
$ helm upgrade \
   --install federation-trust-ui id4me-charts/federation-trust-ui \
   --version <CHART VERSION> \
   --namespace=<NAMESPACE> \
   --wait \
   --timeout=600s

Release "federation-trust-ui" has been upgraded. Happy Helming!
STATUS: deployed
```
For passing user specified values instead of default add --values &lt;VALUES YAML FILE&gt; to the command above.  

## Documentation of used libs, frameworks and plugins

### UI Framework vaadin 14

- [Vaadin 14 documentation](https://vaadin.com/docs/index.html)

### UI Framework vaadin gradle plugin

- [Vaadin Gradle Plugin](https://github.com/vaadin/vaadin-gradle-plugin)

### Spring Boot reference implementation

- [Spring Boot vaadin reference](https://github.com/vaadin/base-starter-spring-gradle)
