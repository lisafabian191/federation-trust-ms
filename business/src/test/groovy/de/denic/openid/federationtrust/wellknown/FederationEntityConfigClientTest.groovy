/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.wellknown

import com.nimbusds.jose.util.Base64
import de.denic.openid.federationtrust.common.CommonFederationEntityStmt
import de.denic.openid.federationtrust.common.Entity
import de.denic.openid.federationtrust.common.JWSOf
import io.micronaut.http.client.BlockingHttpClient
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import spock.lang.Specification

import static io.micronaut.http.HttpResponse.notFound

class FederationEntityConfigClientTest extends Specification {

  HttpClient httpClientMock = Mock()
  BlockingHttpClient blockingHttpClientMock = Mock()
  FederationEntityConfigClient CUT

  def setup() {
    CUT = new FederationEntityConfigClient.Impl(httpClientMock)
    _ * httpClientMock.toBlocking() >> blockingHttpClientMock
  }

  def '404 querying RFC 8615 compliant endpoint leads to another query against not-compliant endpoint'() {
    given:
    Entity entityWithPath = Entity.ofHost('entity.with/path')

    when:
    Optional<JWSOf<CommonFederationEntityStmt>> optJWSOfEntityStmt = CUT.getInfoFrom(entityWithPath)

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "https://entity.with/.well-known/openid-federation/path" })
            >> { throw new HttpClientResponseException('Not found', notFound()) }
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${entityWithPath.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload('''
{
  "iss":"https://entity.with/path",
  "sub":"https://entity.with/path",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "https://trust.anchor"
  ]
}''')
    optJWSOfEntityStmt.isPresent()
    with(optJWSOfEntityStmt.get()) {
      with(payload) {
        authorityHints == Set.of(Entity.ofURI(URI.create('https://trust.anchor')))
        maxPathLength.isEmpty()
      }
    }
  }

  def "Available value of '/constraints/max_path_length' is provided"() {
    given:
    Entity entityWithMaxPathLength = Entity.ofHost('entity.max.path.length')

    when:
    Optional<JWSOf<CommonFederationEntityStmt>> optJWSOfEntityStmt = CUT.getInfoFrom(entityWithMaxPathLength)

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "https://entity.max.path.length/.well-known/openid-federation" })
            >> dummyJWSWithPayload('''
{
  "iss":"https://entity.with/path",
  "sub":"https://entity.with/path",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "constraints": {
    "max_path_length": 42
  }
}''')
    optJWSOfEntityStmt.isPresent()
    with(optJWSOfEntityStmt.get()) {
      with(payload) {
        maxPathLength.orElse(null) == 42
        authorityHints.isEmpty()
      }
    }
  }

  static String dummyJWSWithPayload(String payload) {
    def dummyHeader = '{"alg":"HS256"}'
    def dummySignature = 'DummySignature'
    "${Base64.encode(dummyHeader)}.${Base64.encode(payload)}.${dummySignature}"
  }

}
