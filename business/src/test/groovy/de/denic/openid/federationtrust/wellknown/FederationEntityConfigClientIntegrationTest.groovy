/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.wellknown

import de.denic.openid.federationtrust.common.CommonFederationEntityStmt
import de.denic.openid.federationtrust.common.Entity
import de.denic.openid.federationtrust.common.JWSOf
import io.micronaut.http.client.HttpClient
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Requires
import spock.lang.Specification

import javax.inject.Inject

import static de.denic.openid.federationtrust.util.Utils.isURLReachable

@MicronautTest
class FederationEntityConfigClientIntegrationTest extends Specification {

  @Inject
  HttpClient httpClient
  FederationEntityConfigClient CUT

  def setup() {
    CUT = new FederationEntityConfigClient.Impl(httpClient)
  }

  def "Empty result querying Entity 'example.com' not having Federation Well-Known endpoint"() {
    expect:
    CUT.getInfoFrom(Entity.ofHost('example.com')).isEmpty()
  }

  @Requires({ isURLReachable('https://oidcfed.igtf.net/.well-known/openid-configuration') })
  def "Result querying Entity 'oidcfed.igtf.net' presents Federation API endpoint"() {
    when:
    def optFederationInfo = CUT.getInfoFrom(Entity.ofHost('oidcfed.igtf.net'))

    then:
    optFederationInfo.isPresent()
    JWSOf<CommonFederationEntityStmt> jwsOfFederationInfo = optFederationInfo.orElseThrow()
    CommonFederationEntityStmt entityStatement = jwsOfFederationInfo.payload
    entityStatement.federationApiEndpoint.isPresent()
  }

}
