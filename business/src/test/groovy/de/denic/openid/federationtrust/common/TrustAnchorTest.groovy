/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.common

import com.nimbusds.jose.jwk.Curve
import com.nimbusds.jose.jwk.JWK
import com.nimbusds.jose.jwk.JWKSet
import com.nimbusds.jose.jwk.gen.ECKeyGenerator
import com.nimbusds.jose.util.Base64
import de.denic.openid.federationtrust.common.TrustAnchor
import spock.lang.Specification

import static com.nimbusds.jose.jwk.KeyUse.SIGNATURE

class TrustAnchorTest extends Specification {

  def 'Trust Anchor with entity only'() {
    when:
    def trustAnchor = TrustAnchor.fromEntityAndJWKSet('example.trust.anchor', null)

    then:
    trustAnchor.entity.plainValue == 'example.trust.anchor'
    trustAnchor.jwkSet.isEmpty()
  }

  def 'No Trust Anchor with JWKSet only'() {
    expect:
    TrustAnchor.fromEntityAndJWKSet('', 'DummyJWKSet') == null
    TrustAnchor.fromEntityAndJWKSet('', null) == null
  }

  def 'Trust Anchor with entity and plain JSON JWKSet (key ID containes spaces)'() {
    given:
    JWK jwkWithIDContainingSpaces = new ECKeyGenerator(Curve.P_256)
            .keyUse(SIGNATURE)
            .keyID('test Key ID')
            .generate()
    def jwkSetWithIDContainingSpacesJSON = new JWKSet(jwkWithIDContainingSpaces).toJSONObject(true).toString()

    when:
    def trustAnchor = TrustAnchor.fromEntityAndJWKSet('example.trust.anchor', jwkSetWithIDContainingSpacesJSON)

    then:
    trustAnchor.jwkSet.get().toJSONObject().toJSONString() == jwkSetWithIDContainingSpacesJSON
  }

  def 'Trust Anchor with entity and base64-encoded JSON JWKSet'() {
    given:
    JWK jwk = new ECKeyGenerator(Curve.P_256)
            .keyUse(SIGNATURE)
            .keyID('testKeyID')
            .generate()
    def jwkSetJSON = new JWKSet(jwk).toJSONObject(true).toString()
    def base64JWKSetJSON = Base64.encode(jwkSetJSON).toString()
    int split = 12
    def base64JWKSetJSONWithSpaces = " ${base64JWKSetJSON.substring(0, split)} ${base64JWKSetJSON.substring(split)} "

    when:
    def trustAnchor = TrustAnchor.fromEntityAndJWKSet('example.trust.anchor', base64JWKSetJSON)

    then:
    trustAnchor.jwkSet.get().toJSONObject().toJSONString() == jwkSetJSON

    when:
    trustAnchor = TrustAnchor.fromEntityAndJWKSet('example.trust.anchor', base64JWKSetJSONWithSpaces)

    then:
    trustAnchor.jwkSet.get().toJSONObject().toJSONString() == jwkSetJSON
  }

}
