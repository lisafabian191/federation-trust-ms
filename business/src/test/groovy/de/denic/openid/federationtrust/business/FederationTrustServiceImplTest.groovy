/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business

import com.nimbusds.jose.util.Base64
import de.denic.openid.federationtrust.common.Entity
import de.denic.openid.federationtrust.wellknown.FederationEntityConfigClient
import io.micronaut.http.client.BlockingHttpClient
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import spock.lang.Ignore
import spock.lang.Specification

import static de.denic.openid.federationtrust.common.TrustAnchor.fromEntity
import static io.micronaut.http.HttpResponse.notFound

class FederationTrustServiceImplTest extends Specification {

  HttpClient httpClientMock = Mock()
  BlockingHttpClient blockingHttpClientMock = Mock()
  FederationTrustService CUT

  def setup() {
    FederationEntityConfigClient fedEntityConfigInfoClient = new FederationEntityConfigClient.Impl(httpClientMock)
    FederationEntityStmtClient fedEntityStmtClient = new FederationEntityStmtClient.Impl(httpClientMock, fedEntityConfigInfoClient)
    CUT = new FederationTrustService.Impl(fedEntityConfigInfoClient, fedEntityStmtClient)
    _ * httpClientMock.toBlocking() >> blockingHttpClientMock
  }

  def 'Leaf entity provides NO Federation Entity Config'() {
    given:
    Entity leaf = Entity.ofHost('leaf.without.config')

    when:
    TrustChain trustChain = CUT.seekForChainOf(leaf).now()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> { throw new HttpClientResponseException('Not found', notFound()) }
    with(trustChain.leafNode) {
      description == 'NO Federation Entity Config available.'
      nodesToFollow.isEmpty()
    }
    !trustChain.isTrustAnchorReached()
    !trustChain.immediateVerificationSucceeds()
  }

  def "Leaf entity provides EXPIRED Federation Entity Config Info"() {
    given:
    Entity leaf = Entity.ofHost('leaf.config.expired')

    when:
    TrustChain trustChain = CUT.seekForChainOf(leaf).now()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"irrelevant",
  "iat":1262304000,
  "exp":1420070400,
  "jwks":{
    "keys":[]
  }
}""")
    with(trustChain.leafNode) {
      description == 'Federation Entity Config currently invalid: [2010-01-01T00:00Z[UTC],2015-01-01T00:00Z[UTC]].'
      nodesToFollow.isEmpty()
    }
    !trustChain.isTrustAnchorReached()
    !trustChain.immediateVerificationSucceeds()
  }

  def 'Querying for Leaf entity also beeing custom Trust Anchor'() {
    given:
    Entity leaf = Entity.ofHost('leaf.also.custom.trust.anchor')
    def customTrustAnchor = fromEntity(leaf)
    Entity authorityHint = Entity.ofURI(URI.create('https://no.trust.anchor'))

    when:
    TrustChain trustChain = CUT.seekForChainOf(leaf)
            .withCustomTrustAnchors(customTrustAnchor)
            .now()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks":{
    "keys":[]
  },
  "authority_hints": [
    "${authorityHint}"
  ]
}""")
    with(trustChain.leafNode) {
      description == 'Authority hints to follow: [https://no.trust.anchor]. Reached custom Trust Anchor.'
      nodesToFollow.isEmpty()
    }
    trustChain.isTrustAnchorReached()
    !trustChain.immediateVerificationSucceeds()
  }

  def 'Querying for Leaf entity also beeing Trust Anchor (having NO authority hints)'() {
    given:
    Entity leaf = Entity.ofHost('leaf.also.trust.anchor')

    when:
    TrustChain trustChain = CUT.seekForChainOf(leaf).now()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks":{
    "keys":[]
  },
  "authority_hints": []
}""")
    with(trustChain.leafNode) {
      description == 'Reached Trust Anchor.'
      nodesToFollow.isEmpty()
    }
    trustChain.isTrustAnchorReached()
    !trustChain.immediateVerificationSucceeds()
  }

  def 'Querying for Entity showing single Authority Hint being custom Trust Anchor'() {
    given:
    Entity leaf = Entity.ofHost('leaf.with.auth-hint')
    Entity trustAnchor = Entity.ofHost('trust.anchor')
    URI trustAnchorsFederationAPI = URI.create('https://trust.anchor/federation_api_endpoint')

    when:
    TrustChain trustChain = CUT.seekForChainOf(leaf).now()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${trustAnchor.asHttpsURI()}"
  ]
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${trustAnchor.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${trustAnchor.asHttpsURI()}",
  "sub":"${trustAnchor.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "metadata": {
    "federation_entity": {
      "federation_api_endpoint":"${trustAnchorsFederationAPI}"
    }
  },
  "jwks": {
    "keys":[]
  },
  "authority_hints": []
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${trustAnchorsFederationAPI}?sub=https%3A%2F%2Fleaf.with.auth-hint&iss=https%3A%2F%2Ftrust.anchor" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${trustAnchor.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "authority_hints": []
}""")
    println trustChain
    with(trustChain.leafNode) {
      description == 'Authority hints to follow: [https://trust.anchor].'
      with(nodesToFollow) {
        size() == 1
        with(get(0)) {
          description == 'Reached Trust Anchor.'
          nodesToFollow.isEmpty()
        }
      }
    }
    trustChain.isTrustAnchorReached()
    !trustChain.immediateVerificationSucceeds()
  }

  def 'Querying for Entity showing single Authority Hint having max-path-length of MINUS ONE'() {
    given:
    Entity leaf = Entity.ofHost('leaf.with.auth-hint')
    Entity trustAnchor = Entity.ofHost('trust.anchor')
    URI trustAnchorsFederationAPI = URI.create('https://trust.anchor/federation_api_endpoint')

    when:
    TrustChain trustChain = CUT.seekForChainOf(leaf).now()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${trustAnchor.asHttpsURI()}"
  ]
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${trustAnchor.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${trustAnchor.asHttpsURI()}",
  "sub":"${trustAnchor.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "metadata": {
    "federation_entity": {
      "federation_api_endpoint":"${trustAnchorsFederationAPI}"
    }
  },
  "jwks": {
    "keys":[]
  },
  "constraints": {
    "max_path_length": -1
  },
  "authority_hints": [
    "foo.bar.baz"
  ]
}""")
    println trustChain
    with(trustChain.leafNode) {
      description == 'Authority hints to follow: [https://trust.anchor].'
      with(nodesToFollow) {
        size() == 1
        with(get(0)) {
          description == 'Authority hints to follow: [foo.bar.baz]. Max path length exceeded: 0 > -1.'
          nodesToFollow.isEmpty()
        }
      }
    }
    !trustChain.isTrustAnchorReached()
    !trustChain.immediateVerificationSucceeds()
  }

  @Ignore
  def 'Querying for Entity showing two Authority Hints, both linked to common Trust Anchor'() {
    given:
    Entity leaf = Entity.ofHost('leaf')
    Entity authHint1 = Entity.ofHost('first.auth.hint')
    Entity authHint2 = Entity.ofHost('second.auth.hint')
    Entity commonTrustAnchor = Entity.ofHost('common.trust.anchor')
    URI authHint1FederationAPI = URI.create("${authHint1.asHttpsURI()}/federation_api_endpoint")
    URI authHint2FederationAPI = URI.create("${authHint2.asHttpsURI()}/federation_api_endpoint")
    URI trustAnchorsFederationAPI = URI.create("${commonTrustAnchor.asHttpsURI()}/federation_api_endpoint")

    when:
    TrustChain trustChain = CUT.seekForChainOf(leaf).now()

    then:
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${leaf.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${leaf.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${authHint1.asHttpsURI()}","${authHint2.asHttpsURI()}"
  ]
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${authHint1.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${authHint1.asHttpsURI()}",
  "sub":"${authHint1.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "metadata": {
    "federation_entity": {
      "federation_api_endpoint":"${authHint1FederationAPI}"
    }
  },
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${commonTrustAnchor.asHttpsURI()}"
  ]
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${authHint2.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${authHint2.asHttpsURI()}",
  "sub":"${authHint2.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "metadata": {
    "federation_entity": {
      "federation_api_endpoint":"${authHint2FederationAPI}"
    }
  },
  "jwks": {
    "keys":[]
  },
  "authority_hints": [
    "${commonTrustAnchor.asHttpsURI()}"
  ]
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${commonTrustAnchor.asHttpsURI()}/.well-known/openid-federation" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${commonTrustAnchor.asHttpsURI()}",
  "sub":"${commonTrustAnchor.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "metadata": {
    "federation_entity": {
      "federation_api_endpoint":"${trustAnchorsFederationAPI}"
    }
  },
  "jwks": {
    "keys":[]
  },
  "authority_hints": []
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${authHint1FederationAPI}?sub=https%3A%2F%2F${leaf}&iss=https%3A%2F%2F${authHint1}" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${authHint1.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  }
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${authHint2FederationAPI}?sub=https%3A%2F%2F${leaf}&iss=https%3A%2F%2F${authHint2}" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${authHint2.asHttpsURI()}",
  "sub":"${leaf.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  }
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${trustAnchorsFederationAPI}?sub=https%3A%2F%2F${authHint1}&iss=https%3A%2F%2F${commonTrustAnchor}" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${commonTrustAnchor.asHttpsURI()}",
  "sub":"${authHint1.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  }
}""")
    1 * blockingHttpClientMock.retrieve({ it.getUri().toString() == "${trustAnchorsFederationAPI}?sub=https%3A%2F%2F${authHint2}&iss=https%3A%2F%2F${commonTrustAnchor}" })
            >> dummyJWSWithPayload("""\
{
  "iss":"${commonTrustAnchor.asHttpsURI()}",
  "sub":"${authHint2.asHttpsURI()}",
  "iat":1577836800,
  "exp":4102444800,
  "jwks": {
    "keys":[]
  }
}""")
    println trustChain
    with(trustChain.leafNode) {
      description == 'Authority hints to follow: [https://trust.anchor].'
      with(nodesToFollow) {
        size() == 1
        with(get(0)) {
          description == 'Reached Trust Anchor.'
          nodesToFollow.isEmpty()
        }
      }
    }
    trustChain.isTrustAnchorReached()
    !trustChain.immediateVerificationSucceeds()
  }

  static String dummyJWSWithPayload(String payload) {
    def dummyHeader = '{"kid":"dummy","alg":"HS256"}'
    def dummySignature = 'DummySignature'
    "${Base64.encode(dummyHeader)}.${Base64.encode(payload)}.${dummySignature}"
  }

}
