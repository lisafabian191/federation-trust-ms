/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business

import com.nimbusds.jose.jwk.Curve
import com.nimbusds.jose.jwk.JWK
import com.nimbusds.jose.jwk.JWKSet
import com.nimbusds.jose.jwk.gen.ECKeyGenerator
import de.denic.openid.federationtrust.common.Entity
import de.denic.openid.federationtrust.common.TrustAnchor
import de.denic.openid.federationtrust.wellknown.FederationEntityConfigClient
import io.micronaut.http.client.HttpClient
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Requires
import spock.lang.Specification

import javax.inject.Inject

import static com.nimbusds.jose.jwk.KeyUse.SIGNATURE
import static de.denic.openid.federationtrust.util.Utils.isHTTPsReachable
import static de.denic.openid.federationtrust.util.Utils.isURLReachable

@MicronautTest
class FederationTrustServiceImplIntegrationTest extends Specification {

  @Inject
  HttpClient httpClient
  FederationTrustService CUT

  def setup() {
    def fedEntityConfigInfoClient = new FederationEntityConfigClient.Impl(httpClient)
    def fedEntityStmtClient = new FederationEntityStmtClient.Impl(httpClient, fedEntityConfigInfoClient)
    CUT = new FederationTrustService.Impl(fedEntityConfigInfoClient, fedEntityStmtClient)
  }

  def cleanup() {
    httpClient.stop()
  }

  @Requires({ isURLReachable('https://oidcfed.igtf.net/.well-known/openid-configuration') })
  def "Seeking chain of Trust Anchor 'oidcfed.igtf.net'"() {
    given:
    Entity entity = Entity.ofHost('oidcfed.igtf.net')

    when:
    TrustChain trustChain = CUT.seekForChainOf(entity).now()

    then:
    with(trustChain.leafNode) {
      description == "Mismatching issuer: 'oidcfed.igtf.net' <> 'https://oidcfed.igtf.net/root' (from Federation Entity Config)."
      with(nodesToFollow) {
        isEmpty()
      }
    }
    !trustChain.isTrustAnchorReached()
    !trustChain.immediateVerificationSucceeds()
  }

  def "Seeking chain of 'example.com' providing no Well-Known endpoint"() {
    given:
    Entity entity = Entity.ofHost('example.com')

    when:
    TrustChain trustChain = CUT.seekForChainOf(entity).now()

    then:
    with(trustChain.leafNode) {
      description == 'NO Federation Entity Config available.'
      with(nodesToFollow) {
        isEmpty()
      }
    }
    !trustChain.isTrustAnchorReached()
    !trustChain.immediateVerificationSucceeds()
  }

  @Requires({ isHTTPsReachable('fapi.c2id.com') })
  def "Seeking chain of 'fapi.c2id.com' reaching Trust Anchor after two steps"() {
    given:
    Entity entity = Entity.ofHost('fapi.c2id.com')
    def authorityHintOfEntity = Entity.ofURI(URI.create('https://federation.catalogix.se:4002/eid/umu.se'))
    def authorityHintOfAuthorityHint = Entity.ofURI(URI.create('https://federation.catalogix.se:4002/eid/swamid.se'))

    when:
    TrustChain trustChain = CUT.seekForChainOf(entity).now()

    then:
    with(trustChain.leafNode) {
      description == "Authority hints to follow: [${authorityHintOfEntity}]."
      with(nodesToFollow) {
        size() == 1
        with(get(0)) {
          description == "Authority hints to follow: [${authorityHintOfAuthorityHint}]."
          with(nodesToFollow) {
            size() == 1
            with(get(0)) {
              description == 'Reached Trust Anchor.'
              nodesToFollow.isEmpty()
            }
          }
        }
      }
    }
    trustChain.isTrustAnchorReached()
    trustChain.immediateVerificationSucceeds()
  }

  @Requires({ isHTTPsReachable('fapi.c2id.com') })
  def "Seeking chain of 'fapi.c2id.com' with custom Trust Anchor at first step"() {
    given:
    Entity entity = Entity.ofHost('fapi.c2id.com')
    def authorityHintOfEntity = Entity.ofURI(URI.create('https://federation.catalogix.se:4002/eid/umu.se'))
    def authorityHintOfAuthorityHint = Entity.ofURI(URI.create('https://federation.catalogix.se:4002/eid/swamid.se'))
    TrustAnchor customTrustAnchor = new TrustAnchor(authorityHintOfEntity, null)

    when:
    TrustChain trustChain = CUT.seekForChainOf(entity)
            .withCustomTrustAnchors(customTrustAnchor)
            .now()

    then:
    with(trustChain.leafNode) {
      description == "Authority hints to follow: [${authorityHintOfEntity}]."
      with(nodesToFollow) {
        size() == 1
        with(get(0)) {
          description == "Authority hints to follow: [${authorityHintOfAuthorityHint}]. Reached custom Trust Anchor."
          with(nodesToFollow) {
            size() == 0
          }
        }
      }
    }
    trustChain.isTrustAnchorReached()
    trustChain.immediateVerificationSucceeds()
  }

  @Requires({ isHTTPsReachable('fapi.c2id.com') })
  def "Seeking chain of 'fapi.c2id.com' with custom Trust Anchor and artifical JWS set fails"() {
    given:
    Entity entity = Entity.ofHost('fapi.c2id.com')
    def authorityHintOfEntity = Entity.ofURI(URI.create('https://federation.catalogix.se:4002/eid/umu.se'))
    def authorityHintOfAuthorityHint = Entity.ofURI(URI.create('https://federation.catalogix.se:4002/eid/swamid.se'))
    JWK artificialJWK = new ECKeyGenerator(Curve.P_256)
            .keyUse(SIGNATURE)
            .keyID('artificialKey')
            .generate()
    TrustAnchor customTrustAnchor = new TrustAnchor(authorityHintOfEntity, new JWKSet(artificialJWK))

    when:
    TrustChain trustChain = CUT.seekForChainOf(entity)
            .withCustomTrustAnchors(customTrustAnchor)
            .now()

    then:
    with(trustChain.leafNode) {
      description == "Authority hints to follow: [${authorityHintOfEntity}]."
      with(nodesToFollow) {
        size() == 1
        with(get(0)) {
          description == "Authority hints to follow: [${authorityHintOfAuthorityHint}]. Reached custom Trust Anchor."
          with(nodesToFollow) {
            size() == 0
          }
        }
      }
    }
    trustChain.isTrustAnchorReached()
    !trustChain.immediateVerificationSucceeds()
  }

  def "Seeking chain of 'id.staging.denic.de' with itself as custom Trust Anchor"() {
    given:
    Entity entity = Entity.ofHost('id.staging.denic.de')
    def sameEntityAsAuthHint = Entity.ofURI(entity.asHttpsURI())
    TrustAnchor customTrustAnchor = new TrustAnchor(sameEntityAsAuthHint, null)

    when:
    TrustChain trustChain = CUT.seekForChainOf(entity)
            .withCustomTrustAnchors(customTrustAnchor)
            .now()

    then:
    with(trustChain.leafNode) {
      description == "Authority hints to follow: [${sameEntityAsAuthHint}/dummy/trust-anchor]. Reached custom Trust Anchor."
      with(nodesToFollow) {
        it.isEmpty()
      }
    }
    trustChain.isTrustAnchorReached()
    trustChain.immediateVerificationSucceeds()
  }

}
