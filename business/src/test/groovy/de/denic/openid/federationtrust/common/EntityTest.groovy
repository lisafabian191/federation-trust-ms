/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.common

import spock.lang.Specification

class EntityTest extends Specification {

  def 'toString() prints value'() {
    given:
    def entityFromValue = Entity.ofHost('host.value')
    def entityFromURI = Entity.ofURI(URI.create('https://uri.value'))

    expect:
    entityFromValue.toString() == 'host.value'
    entityFromURI.toString() == 'https://uri.value'
  }

  def 'Equality established on URI value'() {
    given:
    def entityFromURI = Entity.ofURI(URI.create('https://one.uri.value'))
    def entityFromSameValue = Entity.ofHost('one.uri.value')
    def entityFromOtherValue = Entity.ofHost('other.uri.value')

    expect:
    entityFromURI == entityFromSameValue
    entityFromURI != entityFromOtherValue
    entityFromSameValue == entityFromURI
    entityFromOtherValue != entityFromURI
    // Equals/hashCode contract:
    entityFromURI.hashCode() == entityFromSameValue.hashCode()
  }

  def 'Handling of paths in plain or URI value'() {
    given:
    def entityWithPathFromURI = Entity.ofURI(URI.create('https://entity.host/path'))
    def entityWithPathFromString = Entity.ofHost('entity.host/path')

    expect:
    entityWithPathFromURI == entityWithPathFromString
    entityWithPathFromURI.asHttpsURI() == entityWithPathFromString.asHttpsURI()
    entityWithPathFromURI.asHttpsURI().toString() == 'https://entity.host/path'
    // Equals/hashCode contract:
    entityWithPathFromURI.hashCode() == entityWithPathFromString.hashCode()
  }

}
