/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business

import com.nimbusds.jose.jwk.Curve
import com.nimbusds.jose.jwk.JWK
import com.nimbusds.jose.jwk.JWKSet
import com.nimbusds.jose.jwk.gen.ECKeyGenerator
import de.denic.openid.federationtrust.common.Entity
import de.denic.openid.federationtrust.common.TrustAnchor
import de.denic.openid.federationtrust.wellknown.FederationEntityConfigClient
import io.micronaut.http.client.HttpClient
import io.micronaut.test.extensions.spock.annotation.MicronautTest
import spock.lang.Requires
import spock.lang.Specification

import javax.inject.Inject

import static com.nimbusds.jose.jwk.KeyUse.SIGNATURE
import static de.denic.openid.federationtrust.util.Utils.isHTTPsReachable
import static de.denic.openid.federationtrust.util.Utils.isURLReachable

@MicronautTest
class FederationTrustServiceImplViaGraphIntegrationTest extends Specification {

  @Inject
  HttpClient httpClient
  FederationTrustService CUT

  def setup() {
    def fedEntityConfigInfoClient = new FederationEntityConfigClient.Impl(httpClient)
    def fedEntityStmtClient = new FederationEntityStmtClient.Impl(httpClient, fedEntityConfigInfoClient)
    CUT = new FederationTrustService.Impl(fedEntityConfigInfoClient, fedEntityStmtClient)
  }

  def cleanup() {
    httpClient.stop()
  }

  @Requires({ isURLReachable('https://oidcfed.igtf.net/.well-known/openid-configuration') })
  def "Seeking graph of Trust Anchor 'oidcfed.igtf.net'"() {
    given:
    Entity entity = Entity.ofHost('oidcfed.igtf.net')

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(entity).graphNow()

    then:
    with(trustGraph.getLeafVertex()) {
      it.getIssues() == ["Mismatching issuer: 'oidcfed.igtf.net' <> 'https://oidcfed.igtf.net/root' (from Federation Entity Config)."]
      it.getMessages().isEmpty()
    }
    with(trustGraph.getGraph()) {
      it.vertexSet().size() == 1
      it.edgeSet().isEmpty()
    }
    !trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

  def "Seeking graph of 'example.com' providing no Well-Known endpoint"() {
    given:
    Entity entity = Entity.ofHost('example.com')

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(entity).graphNow()

    then:
    with(trustGraph.getLeafVertex()) {
      it.getIssues() == ['NO Federation Entity Config available.']
      it.getMessages().isEmpty()
    }
    with(trustGraph.getGraph()) {
      it.vertexSet().size() == 1
      it.edgeSet().isEmpty()
    }
    !trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

  @Requires({ isHTTPsReachable('fapi.c2id.com') })
  def "Seeking graph of 'fapi.c2id.com' reaching Trust Anchor after two steps"() {
    given:
    Entity entity = Entity.ofHost('fapi.c2id.com')
    def authorityHintOfEntity = Entity.ofURI(URI.create('https://federation.catalogix.se:4002/eid/umu.se'))
    def authorityHintOfAuthorityHint = Entity.ofURI(URI.create('https://federation.catalogix.se:4002/eid/swamid.se'))

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(entity).graphNow()

    then:
    with(trustGraph.getLeafVertex()) {
      it.getIssues().isEmpty()
      it.getMessages() == ["Successful verification of self-signed Federation Entity Configuration.",
                           "Federation Entity Configuration verified with JWK (ID 'exR5') of authority '${authorityHintOfEntity}'."]
      it.isNoTrustAnchor()
    }
    with(trustGraph.getValidPathsToTrustAnchors()) {
      it.size() == 1
      with(it.get(0)) {
        with(it.getVertexList()) {
          it.size() == 3
          it.get(0) == trustGraph.getLeafVertex()
          with(it.get(1)) {
            it.getIssues().isEmpty()
            it.getMessages() == ["Successful verification of self-signed Federation Entity Configuration.",
                                 "Federation Entity Configuration verified with JWK (ID 'UDA3Z1BYRGxYeWpQQ1RTZE81NWR4d1lxN3g4UzRrTG9LVEV2V3pDck5IZw') of authority '${authorityHintOfAuthorityHint}'."]
            it.isNoTrustAnchor()
          }
          with(it.get(2)) {
            it.getIssues().isEmpty()
            it.getMessages() == ["Reached generic Trust Anchor.",
                                 "Successful verification of self-signed Federation Entity Configuration."]
            it.isTrustAnchor()
          }
        }
        it.getEdgeList().size() == 2
      }
    }
    trustGraph.isTrustAnchorReached()
    trustGraph.verificationHasSucceeded()
  }

  @Requires({ isHTTPsReachable('fapi.c2id.com') })
  def "Seeking graph of 'fapi.c2id.com' with custom Trust Anchor at first step"() {
    given:
    Entity entity = Entity.ofHost('fapi.c2id.com')
    def authorityHintOfEntity = Entity.ofURI(URI.create('https://federation.catalogix.se:4002/eid/umu.se'))
    TrustAnchor customTrustAnchor = new TrustAnchor(authorityHintOfEntity, null)

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(entity)
            .withCustomTrustAnchors(customTrustAnchor)
            .graphNow()

    then:
    with(trustGraph.getValidPathsToTrustAnchors()) {
      it.size() == 1
      with(it.get(0)) {
        with(it.getVertexList()) {
          it.size() == 2
          it.get(0) == trustGraph.getLeafVertex()
          with(it.get(1)) {
            it.getIssues().isEmpty()
            it.getMessages() == ["Reached custom Trust Anchor.",
                                 "Successful verification of self-signed Federation Entity Configuration."]
            it.isTrustAnchor()
          }
        }
        it.getEdgeList().size() == 1
      }
    }
    trustGraph.isTrustAnchorReached()
    trustGraph.verificationHasSucceeded()
  }

  @Requires({ isHTTPsReachable('fapi.c2id.com') })
  def "Seeking graph of 'fapi.c2id.com' with custom Trust Anchor and artificial JWS set fails"() {
    given:
    Entity entity = Entity.ofHost('fapi.c2id.com')
    def authorityHintOfEntity = Entity.ofURI(URI.create('https://federation.catalogix.se:4002/eid/umu.se'))
    JWK artificialJWK = new ECKeyGenerator(Curve.P_256)
            .keyUse(SIGNATURE)
            .keyID('artificialKey')
            .generate()
    TrustAnchor customTrustAnchor = new TrustAnchor(authorityHintOfEntity, new JWKSet(artificialJWK))

    when:
    TrustGraph trustGraph = CUT.seekForChainOf(entity)
            .withCustomTrustAnchors(customTrustAnchor)
            .graphNow()

    then:
    with(trustGraph.getValidPathsToTrustAnchors()) {
      it.size() == 1
      with(it.get(0)) {
        with(it.getVertexList()) {
          it.size() == 2
          it.get(0) == trustGraph.getLeafVertex()
          with(it.get(1)) {
            it.getIssues() == ["Verification of Federation Entity Configuration with custom JWK set failed."]
            it.getMessages() == ["Reached custom Trust Anchor."]
            it.isTrustAnchor()
          }
        }
        it.getEdgeList().size() == 1
      }
    }
    trustGraph.isTrustAnchorReached()
    !trustGraph.verificationHasSucceeded()
  }

}
