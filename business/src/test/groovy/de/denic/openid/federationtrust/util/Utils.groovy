/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.util

import groovy.transform.Memoized

import java.util.concurrent.TimeUnit

class Utils {

  private static final int HTTPS_PORT = 443
  private static final int ONE_SECOND_IN_MILLIS = (int) TimeUnit.SECONDS.toMillis(1L)

  @Memoized
  static boolean isHTTPsReachable(final String host) {
    try (Socket socket = new Socket()) {
      socket.connect(new InetSocketAddress(host, HTTPS_PORT), ONE_SECOND_IN_MILLIS)
      true
    } catch (final SocketTimeoutException ignored) {
      println '==========================================================================================='
      println "WARNING: Skipping test-case cause host '${host}' NOT reachable on HTTPs port ${HTTPS_PORT}!"
      println '==========================================================================================='
      false
    }
  }

  @Memoized
  static boolean isURLReachable(final String urlValue) {
    def url = new URL(urlValue)
    try {
      URLConnection connection = url.openConnection()
      connection.setConnectTimeout(ONE_SECOND_IN_MILLIS)
      connection.content != null
    } catch (final IOException ignored) {
      println '==========================================================================================='
      println "WARNING: Skipping test-case cause URL '${url}' NOT reachable!"
      println '==========================================================================================='
      false
    }
  }

}
