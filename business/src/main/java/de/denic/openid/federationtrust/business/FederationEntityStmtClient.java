/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business;

import de.denic.openid.federationtrust.common.Entity;
import de.denic.openid.federationtrust.common.JWSOf;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfig;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfigClient;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.exceptions.HttpClientException;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.http.uri.UriBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.core.annotation.NonNull;
import javax.annotation.concurrent.Immutable;
import javax.inject.Singleton;
import java.net.URI;
import java.util.Optional;

import static de.denic.openid.federationtrust.util.LogUtil.shortenBodyOf;
import static io.micronaut.http.HttpRequest.GET;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public interface FederationEntityStmtClient {

  /**
   * @param issuer  Required to be different from {@code subject}
   * @param subject Required to be different from {@code issuer}
   */
  @NonNull
  Optional<JWSOf<FederationEntityStmt>> getStatementFrom(@NonNull Entity issuer,
                                                         @NonNull URI issuersFederationApiEndpointURI,
                                                         @NonNull Entity subject);

  /**
   * Short-curcuiting following invocation chain:
   * <ol>
   *   <li>Querying <code>issuer</code>'s config endpoint via {@link FederationEntityConfigClient#getInfoFrom(Entity)}</li>
   *   <li>Extracting <code>issuer</code>'s {@link FederationEntityConfig#getFederationApiEndpoint()}</li>
   *   <li>Querying <code>issuer</code>'s Federation API Endpoint for <code>subject</code> via {@link #getStatementFrom(Entity, URI, Entity)}</li>
   * </ol>
   */
  @NonNull
  QueryBuilder queryIssuer(@NonNull Entity issuer);

  @Immutable
  @Singleton
  final class Impl implements FederationEntityStmtClient {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);

    private final FederationEntityConfigClient federationEntityConfigInfoClient;
    private final HttpClient httpClient;

    public Impl(@NonNull final HttpClient httpClient,
                @NonNull final FederationEntityConfigClient federationEntityConfigInfoClient) {
      this.httpClient = requireNonNull(httpClient, "Missing HTTP client component");
      this.federationEntityConfigInfoClient = requireNonNull(federationEntityConfigInfoClient, "Missing Federation Entity Config Info Client component");
    }

    public Impl(@NonNull final HttpClient httpClient) {
      this(httpClient, new FederationEntityConfigClient.Impl(httpClient));
    }

    @NonNull
    @Override
    public Optional<JWSOf<FederationEntityStmt>> getStatementFrom(@NonNull final Entity issuer,
                                                                  @NonNull final URI issuersFederationApiEndpointURI,
                                                                  @NonNull final Entity subject) {
      if (requireNonNull(issuer, "Missing issuer").equals(subject)) {
        throw new IllegalArgumentException("Same issuer and subject: " + issuer);
      }

      final URI federationApiEndpointFetchingURI = UriBuilder.of(requireNonNull(issuersFederationApiEndpointURI, "Missing Federation API Endpoint URI"))
              .queryParam("iss", issuer.asHttpsURI())
              .queryParam("sub", requireNonNull(subject, "Missing Subject").asHttpsURI())
              .build();
      try {
        final String responseBody = httpClient.toBlocking().retrieve(
                GET(federationApiEndpointFetchingURI));
        final JWSOf<FederationEntityStmt> jwsOfEntityStatement = JWSOf.from(responseBody, FederationEntityStmt.class);
        LOG.info("Queried '{}' for Issuer '{}' regarding Subject '{}': {}",
                issuersFederationApiEndpointURI, issuer, subject, jwsOfEntityStatement);
        return of(jwsOfEntityStatement);
      } catch (final HttpClientResponseException e) {
        final HttpStatus httpStatus = e.getStatus();
        LOG.info("Querying '{}' for Issuer '{}' regarding Subject '{}' failed. Response code '{} {}', body: '{}'",
                issuersFederationApiEndpointURI, issuer, subject, httpStatus.getCode(), httpStatus.getReason(), shortenBodyOf(e.getResponse()));
      } catch (final HttpClientException e) {
        LOG.info("Querying '{}' for Issuer '{}' regarding Subject '{}' failed: {} ({})",
                issuersFederationApiEndpointURI, issuer, subject, e.getMessage(), e.getClass().getName());
      } catch (final RuntimeException e) {
        LOG.warn("Querying '{}' for Issuer '{}' regarding Subject '{}' failed.",
                issuersFederationApiEndpointURI, issuer, subject, e);
      }
      return empty();
    }

    @NonNull
    @Override
    public QueryBuilder queryIssuer(@NonNull final Entity issuer) {
      return new QueryBuilderImpl(issuer);
    }

    @Immutable
    private final class QueryBuilderImpl implements QueryBuilder {

      private final Entity issuer;

      private QueryBuilderImpl(@NonNull final Entity issuer) {
        this.issuer = requireNonNull(issuer, "Missing Issuer");
      }

      @NonNull
      @Override
      public Optional<JWSOf<FederationEntityStmt>> forStmtRegardingSubject(@NonNull final Entity subject) {
        return federationEntityConfigInfoClient.getInfoFrom(issuer)
                .map(JWSOf::getPayload)
                .flatMap(FederationEntityConfig::getFederationApiEndpoint)
                .flatMap(federationApiEndpoint -> getStatementFrom(issuer, federationApiEndpoint, subject));
      }

    }

  }

  interface QueryBuilder {

    @NonNull
    Optional<JWSOf<FederationEntityStmt>> forStmtRegardingSubject(@NonNull Entity subject);

  }

}
