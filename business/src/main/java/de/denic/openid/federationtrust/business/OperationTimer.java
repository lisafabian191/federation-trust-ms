/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business;

import de.denic.openid.federationtrust.common.Interval;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.function.Supplier;

import static de.denic.openid.federationtrust.common.Interval.EMPTY;
import static java.time.ZoneOffset.UTC;
import static java.time.ZonedDateTime.now;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.util.Objects.requireNonNull;

public interface OperationTimer {

  @NonNull
  Interval getOperationInterval();

  @ThreadSafe
  class Impl implements OperationTimer {

    private final TemporalUnit truncation;
    private final Object guard = new Object();
    @GuardedBy("guard")
    private ZonedDateTime startsAt;
    @GuardedBy("guard")
    private ZonedDateTime endsAt;

    /**
     * @param truncation Optional, defaults to {@link ChronoUnit#MILLIS}
     */
    public Impl(@Nullable final TemporalUnit truncation) {
      this.truncation = (truncation == null ? MILLIS : truncation);
    }

    /**
     * @see #Impl(TemporalUnit)
     */
    public Impl() {
      this(null);
    }

    @NonNull
    public final <RESULT> RESULT timed(@NonNull final Supplier<? extends RESULT> supplier) {
      requireNonNull(supplier, "Missing supplier"); // If check fails, no timing occurs!
      startsNow();
      try {
        return requireNonNull(supplier.get(), "Missing result");
      } finally {
        endsNow();
      }
    }

    private void startsNow() {
      synchronized (guard) {
        this.startsAt = now(UTC).truncatedTo(truncation);
      }
    }

    private void endsNow() {
      synchronized (guard) {
        this.endsAt = now(UTC).truncatedTo(truncation);
      }
    }

    @Override
    @NonNull
    public final Interval getOperationInterval() {
      synchronized (guard) {
        return (startsAt == null || endsAt == null ? EMPTY
                : new Interval(startsAt, endsAt));
      }
    }

    @Override
    public String toString() {
      return getOperationInterval().toString();
    }

  }

}
