/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nimbusds.jose.jwk.JWKSet;
import de.denic.openid.federationtrust.common.*;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfig;
import net.minidev.json.JSONObject;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.net.URI;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Stream;

import static de.denic.openid.federationtrust.business.FederationTrustService.MDC_INITIAL_PATH_LENGTH_VALUE;
import static de.denic.openid.federationtrust.common.Interval.EMPTY;
import static de.denic.openid.federationtrust.common.TenthOfSecond.TENTH_OF_SECOND;
import static de.denic.openid.federationtrust.common.TrustAnchor.NO_TRUST_ANCHORS;
import static java.lang.Integer.MAX_VALUE;
import static java.time.ZoneOffset.UTC;
import static java.time.ZonedDateTime.now;
import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

@ThreadSafe
public final class TrustChain {

  private final ZonedDateTime started;
  private volatile ZonedDateTime finished;
  private final int maxPathLength;
  private final LeafNode leafNode;
  private final Map<Entity, TrustAnchor> customTrustAnchorEntitesMappedTrustAnchors;
  private final Object entitiesMappedToNodesGuard = new Object();
  @GuardedBy("entitiesMappedToNodesGuard")
  private final Map<Entity, Node> entitiesMappedToNodes = new HashMap<>();
  private volatile Interval validityInterval;

  public TrustChain(@NonNull final Entity leaf,
                    @Nullable final Collection<TrustAnchor> customTrustAnchors,
                    final int maxPathLength) {
    this.customTrustAnchorEntitesMappedTrustAnchors = requireNonNullElse(customTrustAnchors, NO_TRUST_ANCHORS).stream()
            .filter(Objects::nonNull)
            .collect(toUnmodifiableMap(TrustAnchor::getEntity, identity()));
    this.leafNode = createAndRememberLeafNode(leaf);
    this.started = now().withZoneSameInstant(UTC);
    this.finished = started;
    this.maxPathLength = maxPathLength;
    this.validityInterval = EMPTY;
  }

  @NonNull
  private LeafNode createAndRememberLeafNode(@NonNull final Entity entity) {
    final LeafNode leafNode = new LeafNode(entity);
    synchronized (entitiesMappedToNodesGuard) {
      entitiesMappedToNodes.put(entity, leafNode);
    }
    return leafNode;
  }

  @NonNull
  private IntermediateNode createAndRememberIntermediateNode(@NonNull final Entity entity,
                                                             @NonNull final Integer pathLength,
                                                             @NonNull final Node parent) {
    final IntermediateNode intermediateNode = new IntermediateNode(entity, pathLength, parent);
    synchronized (entitiesMappedToNodesGuard) {
      entitiesMappedToNodes.put(entity, intermediateNode);
    }
    return intermediateNode;
  }

  @NonNull
  public LeafNode getLeafNode() {
    return leafNode;
  }

  @NonNull
  public Entity getLeaf() {
    return leafNode.getEntity();
  }

  @NonNull
  public Collection<TrustAnchor> getCustomTrustAnchors() {
    return customTrustAnchorEntitesMappedTrustAnchors.values();
  }

  public int getMaxPathLength() {
    return maxPathLength;
  }

  private boolean isCustomTrustAnchor(@Nullable final Entity candidate) {
    return (candidate != null && customTrustAnchorEntitesMappedTrustAnchors.containsKey(candidate));
  }

  private Optional<TrustAnchor> getCustomTrustAnchorOf(@Nullable final Entity candidate) {
    return ofNullable(candidate).map(customTrustAnchorEntitesMappedTrustAnchors::get);
  }

  public boolean isTrustAnchorReached() {
    if (leafNode.isTrustAnchor()) {
      return true;
    }

    final List<Node> copyOfNodes;
    synchronized (entitiesMappedToNodesGuard) {
      copyOfNodes = new ArrayList<>(entitiesMappedToNodes.values());
    }
    return copyOfNodes.stream()
            .anyMatch(Node::isTrustAnchor);
  }

  public boolean verificationHasSucceeded() {
    return validityInterval.notEmpty();
  }

  public boolean immediateVerificationSucceeds() {
    final Interval newValidityInterval = leafNode.validityIntervalIfImmediateVerificationSucceeds();
    this.validityInterval = newValidityInterval;
    return newValidityInterval.notEmpty();
  }

  @Nullable
  public JSONObject getJWKSetOfLeaf() {
    return (isTrustAnchorReached()
            ? leafNode.getJWKSetAsJSON()
            : null);
  }

  @NonNull
  public ZonedDateTime getStarted() {
    return started;
  }

  @NonNull
  public ZonedDateTime getFinished() {
    return finished.withZoneSameInstant(UTC);
  }

  private void setFinishedSoFar() {
    finished = now();
  }

  @NonNull
  public Duration getDuration() {
    return Duration.between(started, finished).truncatedTo(TENTH_OF_SECOND);
  }

  @NonNull
  public Node getLeafOfTrustChain() {
    return leafNode;
  }

  @NonNull
  public Interval getValidityInterval() {
    return validityInterval;
  }

  @Override
  public String toString() {
    return "{" + leafNode.getEntity() +
            ", TrustAnchors=" + customTrustAnchorEntitesMappedTrustAnchors +
            ", Nodes=" + leafNode +
            '}';
  }

  @ThreadSafe
  public abstract class Node {

    private final Entity entity;
    private volatile boolean validTrustAnchor;
    private final Object descriptionBuilderGuard = new Object();
    @GuardedBy("descriptionBuilderGuard")
    private final StringBuilder descriptionBuilder = new StringBuilder();
    private final Object nodesToFollowGuard = new Object();
    @GuardedBy("nodesToFollowGuard")
    private final List<IntermediateNode> nodesToFollow = new LinkedList<>();
    private final Object jwsOfEntityConfigGuard = new Object();
    @GuardedBy("jwsOfEntityConfigGuard")
    private JWSOf<FederationEntityConfig> jwsOfEntityConfig;

    private Node(@NonNull final Entity entity) {
      this.entity = requireNonNull(entity, "Missing entity");
      this.validTrustAnchor = false;
    }

    protected final Optional<JWSOf<FederationEntityConfig>> getJWSOfEntityConfig() {
      synchronized (jwsOfEntityConfigGuard) {
        return ofNullable(jwsOfEntityConfig);
      }
    }

    @NonNull
    public final Entity getEntity() {
      return entity;
    }

    @NonNull
    public final List<IntermediateNode> getNodesToFollow() {
      synchronized (nodesToFollowGuard) {
        return new ArrayList<>(nodesToFollow);
      }
    }

    protected Interval validityIntervalIfImmediateVerificationSucceeds() {
      final Interval validityIntervalOfOwnEntityConfig = getJWSOfEntityConfig().map(JWSOf::getPayload)
              .map(FederationEntityConfig::getValidityInterval)
              .orElse(EMPTY);
      if (isTrustAnchor()) {
        final Optional<JWKSet> optJWKSetOfCustomTrustAnchor = getCustomTrustAnchorOf(entity).flatMap(TrustAnchor::getJwkSet);
        if (optJWKSetOfCustomTrustAnchor.isEmpty()) {
          return validityIntervalOfOwnEntityConfig;
        }

        final JWKSet jwkSetOfCustomTrustAnchor = optJWKSetOfCustomTrustAnchor.get();
        try {
          final String verifyingKeyID;
          synchronized (jwsOfEntityConfigGuard) {
            verifyingKeyID = jwsOfEntityConfig.getVerifyingKeyIDFrom(jwkSetOfCustomTrustAnchor);
          }
          appendDescription("Federation Entity Configuration verifiable with custom Trust Anchor's JWK of ID '" + verifyingKeyID + "'.");
          return validityIntervalOfOwnEntityConfig;
        } catch (final JWSVerificationException e) {
          appendDescription("Federation Entity Configuration NOT verifiable with custom Trust Anchor's JWK set: " + e.getMessage());
          return EMPTY;
        }
      }

      final Optional<JWSOf<FederationEntityConfig>> optOwnJWSOfEntityConfig = getJWSOfEntityConfig();
      final Stream<IntermediateNode> verifiedAuthorityHintsWithSubjectJWKSets = getNodesToFollow().stream()
              .filter(intermediateNode -> intermediateNode.validityIntervalIfImmediateVerificationSucceeds().notEmpty())
              .filter(intermediateNode -> intermediateNode.getJWKSetOfSubject().isPresent());
      final Interval validityIntervalOfValidatingAuthorityHint = verifiedAuthorityHintsWithSubjectJWKSets
              .map(intermediateNode -> {
                try {
                  final String idOfVerifyingJWK = intermediateNode.idOfJWKFromEntityConfigVerifying(optOwnJWSOfEntityConfig.orElse(null));
                  appendDescription("Federation Entity Configuration verified with JWK (ID '" + idOfVerifyingJWK + "') of authority '" + intermediateNode.getEntity() + "'.");
                  return intermediateNode.getJWSOfEntityStmt()
                          .getPayload()
                          .getValidityInterval();
                } catch (final JWSVerificationException e) {
                  return null;
                }
              })
              .filter(Objects::nonNull)
              .filter(Interval::notEmpty)
              .max(Interval::endComparator)
              .orElse(EMPTY);
      if (validityIntervalOfValidatingAuthorityHint.isEmpty()) {
        appendDescription("FAILED verification of Federation Entity Configuration with JWK sets from ALL Authority Hints.");
      }
      return validityIntervalOfValidatingAuthorityHint.mergedWith(validityIntervalOfOwnEntityConfig);
    }

    @Nullable
    public final Integer getMaxPathLengthOfEntity() {
      synchronized (jwsOfEntityConfigGuard) {
        return ofNullable(jwsOfEntityConfig).map(JWSOf::getPayload).flatMap(FederationEntityConfig::getMaxPathLength).orElse(null);
      }
    }

    @NonNull
    protected abstract Integer getIncrementedPathLength();

    @NonNull
    public final String getCurrentPathLengthAsString() {
      return requireNonNullElse(getCurrentPathLength(), MDC_INITIAL_PATH_LENGTH_VALUE).toString();
    }

    @Nullable
    public abstract Integer getCurrentPathLength();

    public final boolean isTrustAnchor() {
      return validTrustAnchor;
    }

    protected final void setValidTrustAnchorReached() {
      validTrustAnchor = true;
    }

    @NonNull
    protected final List<IntermediateNode> getUnhandledNodesToFollow() {
      final Set<Entity> copyOfKnownEntities;
      synchronized (entitiesMappedToNodesGuard) {
        copyOfKnownEntities = new HashSet<>(entitiesMappedToNodes.keySet());
      }
      final List<IntermediateNode> copyOfNodesToFollow = getNodesToFollow();
      return copyOfNodesToFollow.stream()
              .filter(node -> node.isUnhandled() || copyOfKnownEntities.contains(node.getEntity()))
              .collect(toList());
    }

    protected final synchronized void setJWSOfEntityConfig(@Nullable final JWSOf<FederationEntityConfig> jwsOfEntityConfig) {
      if (jwsOfEntityConfig == null) {
        appendDescriptionClearingNodesToFollow("NO Federation Entity Config available");
        return;
      }

      synchronized (jwsOfEntityConfigGuard) {
        if (this.jwsOfEntityConfig != null) {
          throw new IllegalStateException("Federation Entity Config of '" + entity + "' already available");
        }

        createNodesToFollowFrom(jwsOfEntityConfig);
        this.jwsOfEntityConfig = jwsOfEntityConfig;
      }
    }

    protected final void appendDescriptionClearingNodesToFollow(final String value) {
      appendDescription(value);
      clearNodesToFollow();
    }

    protected final void createNodesToFollowFrom(@Nullable final JWSOf<FederationEntityConfig> federationEntityConfig) {
      if (federationEntityConfig == null) {
        appendDescriptionClearingNodesToFollow("Missing Federation Entity Config.");
      } else {
        final Integer incrementedPathLength = getIncrementedPathLength();
        final Set<IntermediateNode> newNodesToFollow = federationEntityConfig.getPayload()
                .getAuthorityHints()
                .stream()
                .map(entity -> createAndRememberIntermediateNode(entity, incrementedPathLength, this))
                .collect(toSet());
        if (newNodesToFollow.isEmpty()) {
          clearNodesToFollow();
        } else {
          synchronized (nodesToFollowGuard) {
            if (incrementedPathLength > maxPathLength) {
              appendDescriptionClearingNodesToFollow("Not following Authority Hints: Incremented path length '" + incrementedPathLength + "' would exceed maximum path length '" + maxPathLength + "'");
            } else {
              appendDescriptionClearingNodesToFollow("Authority hints to follow: " + newNodesToFollow.stream().map(Node::getEntity).collect(toList()));
              nodesToFollow.addAll(newNodesToFollow);
            }
          }
        }
      }
    }

    protected final void clearNodesToFollow() {
      synchronized (nodesToFollowGuard) {
        nodesToFollow.clear();
      }
    }

    protected final void appendDescription(@Nullable final String value) {
      if (value == null) {
        return;
      }

      synchronized (descriptionBuilderGuard) {
        if (descriptionBuilder.length() > 0) {
          descriptionBuilder.append(' ');
        }
        descriptionBuilder.append(value);
        if (!value.endsWith(".")) {
          descriptionBuilder.append('.');
        }
      }
    }

    @NonNull
    public String getDescription() {
      synchronized (descriptionBuilderGuard) {
        return descriptionBuilder.toString();
      }
    }

    @Override
    public String toString() {
      return entity.toString() +
              ", TrustAnchor=" + validTrustAnchor +
              ", EntityConfig=" + jwsOfEntityConfig +
              ", Description=" + descriptionBuilder +
              ", NodesToFollow=" + nodesToFollow;
    }

  }

  @ThreadSafe
  public final class LeafNode extends Node {

    private LeafNode(@NonNull final Entity entity) {
      super(entity);
    }

    @NonNull
    protected Integer getIncrementedPathLength() {
      return 0;
    }

    @Nullable
    public Integer getCurrentPathLength() {
      return null;
    }

    @JsonIgnore
    @Nullable
    public JSONObject getJWKSetAsJSON() {
      return getJWSOfEntityConfig().map(JWSOf::getPayload)
              .map(FederationEntityConfig::getPublicJWKSetOfSubject)
              .map(JWKSet::toJSONObject)
              .orElse(null);
    }

    @Override
    protected Interval validityIntervalIfImmediateVerificationSucceeds() {
      final Interval validityIntervalSoFar = super.validityIntervalIfImmediateVerificationSucceeds();
      if (validityIntervalSoFar.isEmpty()) {
        return EMPTY;
      }

      final boolean correctlySelfSigned = getJWSOfEntityConfig().map(JWSOf::verifySelfSigned)
              .orElse(false);
      if (correctlySelfSigned) {
        appendDescription("Successfully verified self-signed Federation Entity Configuration Info.");
        return validityIntervalSoFar.mergedWith(getJWSOfEntityConfig()
                .map(JWSOf::getPayload)
                .map(FederationEntityConfig::getValidityInterval)
                .orElse(EMPTY));
      }

      appendDescription("Verification of self-signed Federation Entity Configuration Info failed.");
      return EMPTY;
    }

    @NonNull
    public final Collection<IntermediateNode> provideNodesToFollowFrom(@Nullable final JWSOf<FederationEntityConfig> jwsOfEntityConfig) {
      try {
        setJWSOfEntityConfig(jwsOfEntityConfig);
        if (jwsOfEntityConfig != null) {
          final FederationEntityConfig entityConfig = jwsOfEntityConfig.getPayload();
          final Entity issuerOfEntityConfig = entityConfig.getIssuer();
          if (!getEntity().equals(issuerOfEntityConfig)) {
            appendDescriptionClearingNodesToFollow("Mismatching issuer: '" + getEntity() + "' <> '" + issuerOfEntityConfig + "' (from Federation Entity Config).");
          } else if (!entityConfig.isValidNow()) {
            appendDescriptionClearingNodesToFollow("Federation Entity Config currently invalid: " + entityConfig.getValidityInterval() + ".");
          } else if (!entityConfig.isSelfSigned()) {
            appendDescriptionClearingNodesToFollow("Federation Entity Config not self-signed.");
          } else if (entityConfig.isTrustAnchor()) {
            setValidTrustAnchorReached();
            appendDescriptionClearingNodesToFollow("Reached Trust Anchor.");
          } else if (isCustomTrustAnchor(entityConfig.getIssuer())) {
            setValidTrustAnchorReached();
            appendDescriptionClearingNodesToFollow("Reached custom Trust Anchor.");
          }
        }
        return getUnhandledNodesToFollow();
      } finally {
        setFinishedSoFar();
      }
    }

    @Override
    public String toString() {
      return "{" + super.toString() + "}";
    }

  }

  @ThreadSafe
  public final class IntermediateNode extends Node {

    private final Node previousNode;
    private final Integer pathLength;
    private final Object jwsOfEntityStmtGuard = new Object();
    @GuardedBy("jwsOfEntityStmtGuard")
    private JWSOf<FederationEntityStmt> jwsOfEntityStmt;

    private IntermediateNode(@NonNull final Entity entity,
                             @NonNull final Integer pathLength,
                             @NonNull final Node previousNode) {
      super(entity);
      this.pathLength = requireNonNull(pathLength, "Missing path length");
      this.previousNode = requireNonNull(previousNode, "Missing previous node");
    }

    protected boolean isUnhandled() {
      return (getJWSOfEntityConfig().isEmpty() && getJWSOfEntityStmt() == null);
    }

    private JWSOf<FederationEntityStmt> getJWSOfEntityStmt() {
      synchronized (jwsOfEntityStmtGuard) {
        return jwsOfEntityStmt;
      }
    }

    @NonNull
    protected Integer getIncrementedPathLength() {
      return pathLength + 1;
    }

    @Nullable
    public Integer getCurrentPathLength() {
      return pathLength;
    }

    @JsonIgnore
    @NonNull
    public Node getPreviousNode() {
      return previousNode;
    }

    @JsonIgnore
    @NonNull
    public Optional<JWKSet> getJWKSetOfSubject() {
      return ofNullable(getJWSOfEntityStmt())
              .map(JWSOf::getPayload)
              .map(FederationEntityStmt::getPublicJWKSetOfSubject);
    }

    @NonNull
    public Optional<URI> provideFederationAPIEndpointFrom(@Nullable final JWSOf<FederationEntityConfig> jwsOfEntityConfig) {
      try {
        setJWSOfEntityConfig(jwsOfEntityConfig);
        if (jwsOfEntityConfig == null) {
          return empty();
        }

        final FederationEntityConfig entityConfig = jwsOfEntityConfig.getPayload();
        final Integer maxPathLength = entityConfig.getMaxPathLength().orElse(MAX_VALUE);
        if (pathLength > maxPathLength) {
          appendDescriptionClearingNodesToFollow("Max path length exceeded: " + getCurrentPathLengthAsString() + " > " + maxPathLength);
          return empty();
        }

        final Entity issuerOfEntityConfig = entityConfig.getIssuer();
        if (!getEntity().equals(issuerOfEntityConfig)) {
          appendDescriptionClearingNodesToFollow("Mismatching issuer: '" + getEntity() + "' <> '" + issuerOfEntityConfig + "' (from Federation Entity Config).");
          return empty();
        }

        if (!entityConfig.isValidNow()) {
          appendDescriptionClearingNodesToFollow("Federation Entity Config currently invalid: " + entityConfig.getValidityInterval() + ".");
          return empty();
        }

        if (!entityConfig.isSelfSigned()) {
          appendDescriptionClearingNodesToFollow("Federation Entity Config NOT self-signed.");
          return empty();
        }

        if (entityConfig.isTrustAnchor()) {
          setValidTrustAnchorReached();
          appendDescriptionClearingNodesToFollow("Reached Trust Anchor.");
        } else if (isCustomTrustAnchor(issuerOfEntityConfig)) {
          setValidTrustAnchorReached();
          appendDescriptionClearingNodesToFollow("Reached custom Trust Anchor.");
        }
        return entityConfig.getFederationApiEndpoint();
      } finally {
        setFinishedSoFar();
      }
    }

    @NonNull
    public Collection<IntermediateNode> provideNodesToFollowFrom(@Nullable final JWSOf<FederationEntityStmt> jwsOfEntityStmt) {
      try {
        setJWSOfEntityStmt(jwsOfEntityStmt);
        if (jwsOfEntityStmt != null) {
          final FederationEntityStmt entityStmt = jwsOfEntityStmt.getPayload();
          if (!entityStmt.isValidNow()) {
            appendDescriptionClearingNodesToFollow("Federation Entity Stmt currently invalid: " + entityStmt.getValidityInterval() + ".");
          }
          if (entityStmt.isSelfSigned()) {
            appendDescriptionClearingNodesToFollow("Federation Entity Stmt IS self-signed.");
          }
          final Entity issuerOfEntityStmt = entityStmt.getIssuer();
          if (!getEntity().equals(issuerOfEntityStmt)) {
            appendDescriptionClearingNodesToFollow("Mismatching issuer: '" + getEntity() + "' <> '" + issuerOfEntityStmt + "' (from Federation Entity Stmt).");
          }
          final Entity subjectOfEntityStmt = entityStmt.getSubject();
          final Entity issuerOfPreviousNode = getPreviousNode().getEntity();
          if (!subjectOfEntityStmt.equals(issuerOfPreviousNode)) {
            appendDescriptionClearingNodesToFollow("Mismatching subject: '" + issuerOfPreviousNode + "' <> '" + subjectOfEntityStmt + "' (from Federation Entity Stmt).");
          }
        }
        return getUnhandledNodesToFollow();
      } finally {
        setFinishedSoFar();
      }
    }

    private void setJWSOfEntityStmt(final JWSOf<FederationEntityStmt> jwsOfEntityStmt) {
      if (jwsOfEntityStmt == null) {
        appendDescriptionClearingNodesToFollow("NO Federation Entity Stmt available.");
        return;
      }

      synchronized (jwsOfEntityStmtGuard) {
        if (this.jwsOfEntityStmt != null) {
          throw new IllegalStateException("Federation Entity Stmt of '" + getEntity() + "' already available.");
        }

        this.jwsOfEntityStmt = jwsOfEntityStmt;
      }
    }

    @NonNull
    private String idOfJWKFromEntityConfigVerifying(@Nullable final JWSOf<FederationEntityConfig> jwsOfEntityConfigToVerify) throws JWSVerificationException {
      if (jwsOfEntityConfigToVerify == null) {
        throw new JWSVerificationException("Missing JWS of Federation Entity Config to verify.");
      }

      return jwsOfEntityConfigToVerify.getVerifyingKeyIDFrom(getJWKSetOfSubject().orElse(null));
    }

    @Override
    public String toString() {
      return "{" + super.toString() +
              ", EntityStmt=" + jwsOfEntityStmt +
              '}';
    }

  }

}
