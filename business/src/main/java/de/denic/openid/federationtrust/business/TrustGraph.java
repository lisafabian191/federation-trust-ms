/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business;

import com.nimbusds.jose.jwk.JWKSet;
import de.denic.openid.federationtrust.common.*;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfig;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm.SingleSourcePaths;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.AsUnmodifiableGraph;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static de.denic.openid.federationtrust.business.FedEntityStmtEdge.INVALID_PATH_WEIGHT;
import static de.denic.openid.federationtrust.common.Interval.EMPTY;
import static de.denic.openid.federationtrust.common.TenthOfSecond.TENTH_OF_SECOND;
import static java.util.Collections.unmodifiableList;
import static java.util.Comparator.naturalOrder;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@ThreadSafe
public final class TrustGraph {

  private static final double MAX_WEIGHT_OF_VALID_PATHS = 0.5d * INVALID_PATH_WEIGHT;

  private final EntityVertex leafVertex;
  private final Interval validityInterval;
  private final int maxPathLength;
  private final ZonedDateTime started;
  private final ZonedDateTime finished;
  private final FederationTrustService.Impl.TrustChainContext trustChainContext;
  private final AsUnmodifiableGraph<EntityVertex, FedEntityStmtEdge> graph;
  private final Set<EntityVertex> validlyReachedTrustAnchors;
  private final List<GraphPath<EntityVertex, FedEntityStmtEdge>> validPathsToTrustAnchors;

  public TrustGraph(@NonNull final FederationTrustService.Impl.TrustChainContext trustChainContext) {
    this.trustChainContext = requireNonNull(trustChainContext, "Missing context");
    this.graph = trustChainContext.getUnmodifiableGraph();
    this.maxPathLength = trustChainContext.getMaxPathLength();
    this.started = earliestStartTimestampFrom(this.graph);
    this.finished = latestEndTimestampFrom(this.graph);
    this.leafVertex = trustChainContext.getLeafVertex()
            .orElseThrow(() -> new RuntimeException("Internal error: NO vertex of leaf entity available!"));
    final Set<EntityVertex> trustAnchorVertices = this.graph.vertexSet()
            .stream()
            .filter(EntityVertex::isTrustAnchor)
            .collect(toSet());
    final SingleSourcePaths<EntityVertex, FedEntityStmtEdge> pathsOfLeafEntity = new DijkstraShortestPath<>(this.graph)
            .getPaths(leafVertex);
    this.validPathsToTrustAnchors = trustAnchorVertices.stream()
            .map(pathsOfLeafEntity::getPath)
            .filter(path -> path.getWeight() <= MAX_WEIGHT_OF_VALID_PATHS)
            .collect(toList());
    this.validlyReachedTrustAnchors = validPathsToTrustAnchors.stream()
            .map(GraphPath::getEndVertex)
            .collect(toSet());
    this.validityInterval = calculateMaxValidityIntervalFrom(this.validPathsToTrustAnchors);
  }

  @NonNull
  private Interval calculateMaxValidityIntervalFrom(@NonNull final Collection<GraphPath<EntityVertex, FedEntityStmtEdge>> paths) {
    return paths.stream()
            .map(this::calculateValidityIntervalOf)
            .max(Interval::endComparator)
            .orElse(EMPTY);
  }

  @NonNull
  private Interval calculateValidityIntervalOf(@NonNull final GraphPath<EntityVertex, FedEntityStmtEdge> path) {
    final Stream<ValidityConstrained> streamOfPathsOperationTimes = Stream.concat(path.getEdgeList().stream(), path.getVertexList().stream());
    return streamOfPathsOperationTimes.map(ValidityConstrained::getValidityInterval)
            .reduce(Interval::mergedWith)
            .orElse(EMPTY);
  }

  @NonNull
  private ZonedDateTime latestEndTimestampFrom(@NonNull final Graph<EntityVertex, FedEntityStmtEdge> graph) {
    return getOperationIntervalStreamFrom(graph)
            .map(Interval::getEnd)
            .flatMap(Optional::stream)
            .max(naturalOrder())
            .orElseThrow(() -> new RuntimeException("Internal error: NO operation end timestamp available!"));
  }

  @NonNull
  private ZonedDateTime earliestStartTimestampFrom(@NonNull final Graph<EntityVertex, FedEntityStmtEdge> graph) {
    return getOperationIntervalStreamFrom(graph)
            .map(Interval::getStart)
            .flatMap(Optional::stream)
            .min(naturalOrder())
            .orElseThrow(() -> new RuntimeException("Internal error: NO operation start timestamp available!"));
  }

  @NonNull
  private Stream<Interval> getOperationIntervalStreamFrom(@NonNull final Graph<EntityVertex, FedEntityStmtEdge> graph) {
    final Stream<Interval> operationIntervalStreamOfVertices = graph.vertexSet()
            .stream()
            .map(EntityVertex::getOperationInterval);
    final Stream<Interval> operationIntervalStreamOfEdges = graph.edgeSet()
            .stream()
            .map(FedEntityStmtEdge::getOperationInterval);
    return Stream.concat(operationIntervalStreamOfEdges, operationIntervalStreamOfVertices);
  }

  @NonNull
  public EntityVertex getLeafVertex() {
    return leafVertex;
  }

  @NonNull
  public Entity getLeaf() {
    return getLeafVertex().getEntity();
  }

  @NonNull
  public Collection<TrustAnchor> getCustomTrustAnchors() {
    return trustChainContext.getCustomTrustAnchors();
  }

  public int getMaxPathLength() {
    return maxPathLength;
  }

  public boolean isTrustAnchorReached() {
    return !validlyReachedTrustAnchors.isEmpty();
  }

  public boolean verificationHasSucceeded() {
    return validityInterval.notEmpty();
  }

  @Nullable
  public JWKSet getJWKSetOfLeaf() {
    return (isTrustAnchorReached()
            ? getLeafVertex().getJWSOfFederationEntityConfig()
            .map(JWSOf::getPayload)
            .map(FederationEntityConfig::getPublicJWKSetOfSubject)
            .orElse(null)
            : null);
  }

  @NonNull
  public ZonedDateTime getStarted() {
    return started;
  }

  @NonNull
  public ZonedDateTime getFinished() {
    return finished;
  }

  @NonNull
  public Duration getDuration() {
    return Duration.between(started, finished).truncatedTo(TENTH_OF_SECOND);
  }

  @NonNull
  public AsUnmodifiableGraph<EntityVertex, FedEntityStmtEdge> getGraph() {
    return graph;
  }

  @NonNull
  public List<GraphPath<EntityVertex, FedEntityStmtEdge>> getValidPathsToTrustAnchors() {
    return unmodifiableList(validPathsToTrustAnchors);
  }

  @NonNull
  public Interval getValidityInterval() {
    return validityInterval;
  }

  @Override
  public String toString() {
    return "{" + leafVertex.getEntity() +
            ", TrustAnchors=" + getCustomTrustAnchors() +
            ", Validity interval=" + validityInterval +
            ", Graph=" + graph +
            '}';
  }

}
