/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.business;

import com.nimbusds.jose.jwk.JWKSet;
import de.denic.openid.federationtrust.common.*;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfig;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfigClient;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static de.denic.openid.federationtrust.business.FederationTrustService.MDC_INITIAL_PATH_LENGTH_VALUE;
import static de.denic.openid.federationtrust.common.Interval.EMPTY;
import static de.denic.openid.federationtrust.common.TenthOfSecond.TENTH_OF_SECOND;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Collections.*;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

@ThreadSafe
public final class EntityVertex extends OperationTimer.Impl implements ValidityConstrained, Comparable<EntityVertex> {

  private final Entity entity;
  private final FederationTrustService.Impl.TrustChainContext context;
  private final List<String> issues = synchronizedList(new LinkedList<>());
  private final List<String> messages = synchronizedList(new LinkedList<>());
  private final Integer currentPathLength;
  private final Object optJWSOfFedEntityConfigGuard = new Object();
  @GuardedBy("optJWSOfFedEntityConfigGuard")
  private JWSOf<FederationEntityConfig> optJWSOfFedEntityConfig;
  private volatile boolean validTrustAnchor;
  private volatile boolean currentPathLengthExceedsMax;
  private final int preCalculatedHashCode;

  public EntityVertex(@NonNull final Entity entity,
                      @Nullable final Integer currentPathLength,
                      @NonNull final FederationTrustService.Impl.TrustChainContext trustChainContext) {
    super(TENTH_OF_SECOND);
    this.entity = requireNonNull(entity, "Missing entity");
    this.context = requireNonNull(trustChainContext, "Missing context");
    this.preCalculatedHashCode = entity.hashCode();
    this.currentPathLength = currentPathLength;
    this.currentPathLengthExceedsMax = context.exceedsMaxPathLength(currentPathLength);
  }

  @NonNull
  public EntityVertex loadYourConfigIfNotAlreadyDoneVia(@NonNull final FederationEntityConfigClient federationEntityConfigClient) {
    if (currentPathLengthExceedsMax) {
      return addingIssue("Entity's path length exceeds Trust Graph's max. path length: " + currentPathLength + " > " + context.getMaxPathLength());
    }

    synchronized (optJWSOfFedEntityConfigGuard) {
      if (optJWSOfFedEntityConfig != null) {
        // Already loaded!
        return this;
      }

      requireNonNull(federationEntityConfigClient, "Missing Federation Entity Config client");
      final Optional<JWSOf<FederationEntityConfig>> optJWSOfFedEntityConfig = timed(() -> federationEntityConfigClient
              .getInfoFrom(entity));
      if (optJWSOfFedEntityConfig.isEmpty()) {
        return addingIssue("NO Federation Entity Config available.");
      }

      this.optJWSOfFedEntityConfig = optJWSOfFedEntityConfig.get();
    }
    final FederationEntityConfig fedEntityConfig = this.optJWSOfFedEntityConfig.getPayload();
    final Entity issuerOfFedEntityConfig = fedEntityConfig.getIssuer();
    if (!entity.equals(issuerOfFedEntityConfig)) {
      return addingIssue("Mismatching issuer: '" + entity + "' <> '" + issuerOfFedEntityConfig + "' (from Federation Entity Config).");
    }

    if (!fedEntityConfig.isValidNow()) {
      return addingIssue("Federation Entity Config currently invalid: " + fedEntityConfig.getValidityInterval() + ".");
    }

    if (!fedEntityConfig.isSelfSigned()) {
      return addingIssue("Federation Entity Config not self-signed.");
    }

    final Integer maxPathLengthDeclaredByEntity = fedEntityConfig.getMaxPathLength().orElse(MAX_VALUE);
    if ((this.currentPathLength != null) && (this.currentPathLength > maxPathLengthDeclaredByEntity)) {
      this.currentPathLengthExceedsMax = true;
      return addingIssue("Current path length exceeds Entity's declared max. path length: " + this.currentPathLength + " > " + maxPathLengthDeclaredByEntity + ".");
    }

    if (fedEntityConfig.isTrustAnchor()) {
      validTrustAnchor = true;
      return addingMessage("Reached generic Trust Anchor.");
    }

    if (context.isCustomTrustAnchor(fedEntityConfig.getIssuer())) {
      validTrustAnchor = true;
      return addingMessage("Reached custom Trust Anchor.");
    }

    return this;
  }

  /**
   * @return <strong>Empty</strong> if {@link #hasIssues()} or {@link #isTrustAnchor()}, so <strong>not</strong> necessarily the set of Authority Hints from {@link FederationEntityConfig} statement!
   */
  @NonNull
  public Set<Entity> getAuthorityHintsToFollow() {
    return (validTrustAnchor || hasIssues()
            ? emptySet()
            : (optJWSOfFedEntityConfig == null
            ? emptySet()
            : optJWSOfFedEntityConfig.getPayload().getAuthorityHints()));
  }

  public String AuthorityHintsToString() {
    String authHints = "";
    for (Entity entity : getAuthorityHintsToFollow()) {
      authHints += ((authHints.length() > 0) ? "," : "") + entity.getPlainValue();
    }
    return authHints;
  }

  @NonNull
  public FedEntityStmtEdge queryForFedEntityStmtEdgeRegarding(@NonNull final EntityVertex subjectVertex,
                                                              @NonNull final FederationEntityStmtClient federationEntityStmtClient) {
    final Optional<URI> optFederationApiEndpointURI = (optJWSOfFedEntityConfig == null
            ? empty()
            : optJWSOfFedEntityConfig.getPayload().getFederationApiEndpoint());
    return new FedEntityStmtEdge(this, optFederationApiEndpointURI.orElse(null), subjectVertex)
            .loadYourFederationEntityStmtApplying(federationEntityStmtClient);
  }

  @NonNull
  public Entity getEntity() {
    return entity;
  }

  public boolean isTrustAnchor() {
    return validTrustAnchor;
  }

  public boolean isNoTrustAnchor() {
    return !isTrustAnchor();
  }

  public boolean hasIssues() {
    return !isValid();
  }

  public boolean isValid() {
    return issues.isEmpty();
  }

  public boolean notExceedsMaxPathLength() {
    return !currentPathLengthExceedsMax;
  }

  @NonNull
  public List<String> getIssues() {
    return unmodifiableList(issues);
  }

  @NonNull
  public List<String> getMessages() {
    return unmodifiableList(messages);
  }

  @NonNull
  public Integer getIncrementedPathLength() {
    return (currentPathLength == null ? 0 : currentPathLength + 1);
  }

  @NonNull
  public String getPathLengthAsString() {
    return (currentPathLength == null ? MDC_INITIAL_PATH_LENGTH_VALUE
            : Integer.toString(currentPathLength));
  }

  @NonNull
  public Optional<Integer> getCurrentPathLength() {
    return ofNullable(currentPathLength);
  }

  @NonNull
  public Optional<JWSOf<FederationEntityConfig>> getJWSOfFederationEntityConfig() {
    return ofNullable(optJWSOfFedEntityConfig);
  }

  @NonNull
  private EntityVertex addingIssue(@NonNull final String issue) {
    issues.add(requireNonNull(issue, "Missing issue"));
    return this;
  }

  @NonNull
  private EntityVertex addingMessage(@NonNull final String message) {
    messages.add(requireNonNull(message, "Missing message"));
    return this;
  }

  public void verify() {
    final Optional<JWSOf<FederationEntityConfig>> optJWSOfFederationEntityConfig = getJWSOfFederationEntityConfig();
    if (isValid() && optJWSOfFederationEntityConfig.isPresent()) {
      final Optional<JWKSet> optCustomJWKSet = context.getJWKSetIfCustomTrustAnchor(entity);
      if (optCustomJWKSet.isPresent()) {
        try {
          final String idOfVerifyingCustomJWKSet = optJWSOfFederationEntityConfig.get().getVerifyingKeyIDFrom(optCustomJWKSet.get());
          addingMessage("Successful verification of Federation Entity Configuration with Key of ID '" + idOfVerifyingCustomJWKSet + "' from custom JWK set.");
        } catch (final JWSVerificationException e) {
          addingIssue("Verification of Federation Entity Configuration with custom JWK set failed: " + e.getMessage());
        }
      } else {
        try {
          final String selfSignedVerifyingKeyID = optJWSOfFederationEntityConfig.get().getSelfSignedVerifyingKeyID();
          addingMessage("Successful verification of self-signed Federation Entity Configuration with key of ID '" + selfSignedVerifyingKeyID + "'.");
        } catch (final JWSVerificationException e) {
          addingIssue("Verification of self-signed Federation Entity Configuration failed: " + e.getMessage());
        }
      }
    }
  }

  @NonNull
  @Override
  public Interval getValidityInterval() {
    return (hasIssues() ? EMPTY
            : getJWSOfFederationEntityConfig()
            .map(JWSOf::getPayload)
            .map(FederationEntityConfig::getValidityInterval)
            .orElse(EMPTY));
  }

  @Override
  public int compareTo(final EntityVertex o) {
    return entity.compareTo(o.entity);
  }

  /**
   * HINT: It's <strong>very</strong> important for <a href="https://jgrapht.org/guide/VertexAndEdgeTypes">JGraphT library</a> vertices
   * to implement {@link #equals(Object)}/{@link #hashCode()} using identifying field <strong>ONLY</strong>!
   */
  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;

    final EntityVertex that = (EntityVertex) o;

    return entity.equals(that.entity);
  }

  /**
   * @see #equals(Object)
   */
  @Override
  public int hashCode() {
    return preCalculatedHashCode;
  }

  @Override
  public String toString() {
    return "{'" + entity + "'" +
            ", issues: " + issues +
            ", messages: " + messages +
            ", evaluated within " + super.toString() + '}';
  }

}
