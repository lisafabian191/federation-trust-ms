/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.common;

import javax.annotation.concurrent.Immutable;
import java.time.Duration;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalUnit;

/**
 * Code mainly inspired by {@link java.time.temporal.ChronoUnit}.
 */
@Immutable
public final class TenthOfSecond implements TemporalUnit {

  public static final TenthOfSecond TENTH_OF_SECOND = new TenthOfSecond();

  private static final Duration DURATION = Duration.ofMillis(100L);

  private TenthOfSecond() {
    // Intentionally left empty
  }

  @Override
  public Duration getDuration() {
    return DURATION;
  }

  @Override
  public boolean isDurationEstimated() {
    return false;
  }

  @Override
  public boolean isDateBased() {
    return true;
  }

  @Override
  public boolean isTimeBased() {
    return true;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <R extends Temporal> R addTo(final R temporal, final long amount) {
    return (R) temporal.plus(amount, this);
  }

  @Override
  public long between(final Temporal temporal1Inclusive, final Temporal temporal2Exclusive) {
    return temporal1Inclusive.until(temporal2Exclusive, this);
  }

  @Override
  public String toString() {
    return "Tenth Of Second";
  }

}
