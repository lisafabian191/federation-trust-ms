/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.ECDSASigner;
import com.nimbusds.jose.crypto.ECDSAVerifier;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSelector;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.text.ParseException;
import java.util.List;
import java.util.Objects;

import static com.nimbusds.jose.jwk.JWKMatcher.forJWSHeader;
import static com.nimbusds.jose.jwk.KeyType.EC;
import static com.nimbusds.jose.jwk.KeyType.RSA;
import static de.denic.openid.federationtrust.common.JWSOf.JWKAndVerifier.with;
import static java.util.Objects.requireNonNull;

public interface JWSOf<PAYLOAD_TYPE extends JWKSetProvider> {

  ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  @NonNull
  PAYLOAD_TYPE getPayload();

  @NonNull
  JWSObject getJWS();

  default boolean verifySelfSigned() {
    try {
      getSelfSignedVerifyingKeyID();
      return true;
    } catch (JWSVerificationException e) {
      return false;
    }
  }

  default boolean verifyWith(@Nullable final JWKSet jwkSet) {
    try {
      getVerifyingKeyIDFrom(jwkSet);
      return true;
    } catch (JWSVerificationException e) {
      return false;
    }
  }

  @NonNull
  String getSelfSignedVerifyingKeyID() throws JWSVerificationException;

  @NonNull
  String getVerifyingKeyIDFrom(@Nullable JWKSet jwkSet) throws JWSVerificationException;

  @NonNull
  static <PTYPE extends JWKSetProvider> JWSOf<PTYPE> from(@NonNull final String jwsData, @NonNull final Class<PTYPE> payloadType) {
    final JWSObject jwsObject;
    try {
      jwsObject = JWSObject.parse(requireNonNull(jwsData, "Missing JWS data"));
    } catch (final ParseException e) {
      throw new IllegalArgumentException("Parsing JWS failed", e);
    }

    return from(jwsObject, payloadType);
  }

  @NonNull
  static <PTYPE extends JWKSetProvider> JWSOf<PTYPE> from(@NonNull final JWSObject jwsObject, @NonNull final Class<PTYPE> payloadType) {
    final PTYPE payloadData = requireNonNull(jwsObject, "Missing JWS instance").getPayload().toType(payload -> {
      try {
        return OBJECT_MAPPER.readerFor(requireNonNull(payloadType, "Missing type of payload")).readValue(payload.toString());
      } catch (final JsonProcessingException e) {
        throw new IllegalArgumentException("Parsing JWS payload (as '" + payloadType.getSimpleName() + "') failed", e);
      }
    });

    return new Impl<>(payloadData, jwsObject);
  }

  @NonNull
  static <PTYPE extends JWKSetProvider> JWSOf<PTYPE> signed(@NonNull final PTYPE payload,
                                                            @NonNull final JWK jwk) {
    final JWSSigner signer = createMatchingSignerOf(jwk);
    final String serializedPayload;
    try {
      serializedPayload = OBJECT_MAPPER.writerFor(requireNonNull(payload, "Missing payload").getClass())
              .writeValueAsString(payload);
    } catch (final JsonProcessingException e) {
      throw new RuntimeException("Serializing payload '" + payload + "' failed", e);
    }

    final JWSObject jwsObject = new JWSObject(
            new JWSHeader.Builder(JWSAlgorithm.ES256).keyID(jwk.getKeyID()).build(),
            new Payload(serializedPayload));
    try {
      jwsObject.sign(signer);
    } catch (final JOSEException e) {
      throw new RuntimeException("Singing payload '" + payload + "' failed", e);
    }

    return new Impl<>(payload, jwsObject);
  }

  @NonNull
  static JWSSigner createMatchingSignerOf(@NonNull final JWK jwk) {
    final KeyType typeOfJWK = requireNonNull(jwk, "Missing JWK").getKeyType();
    if (!jwk.isPrivate()) {
      throw new IllegalArgumentException("Provided JWK (ID '" + jwk.getKeyID() + "') contains NO private key data required for signing operation.");
    }

    try {
      if (RSA.equals(typeOfJWK)) {
        return new RSASSASigner(jwk.toRSAKey());
      }

      if (EC.equals(typeOfJWK)) {
        return new ECDSASigner(jwk.toECKey());
      }

      throw new IllegalArgumentException("Found NO JWS signer matching JWK of type '" + typeOfJWK + "'");
    } catch (final JOSEException e) {
      throw new RuntimeException("Creating JWS signer matching JWK '" + jwk.toJSONString() + " failed", e);
    }
  }

  @Immutable
  final class Impl<PAYLOAD_TYPE extends JWKSetProvider> implements JWSOf<PAYLOAD_TYPE> {

    private static final Logger LOG = LoggerFactory.getLogger(Impl.class);

    private final JWSObject jws;
    private final PAYLOAD_TYPE payload;

    private Impl(@NonNull final PAYLOAD_TYPE payload,
                 @NonNull final JWSObject jws) {
      this.jws = requireNonNull(jws, "Missing JWS");
      this.payload = requireNonNull(payload, "Missing JWS payload object");
    }

    @Override
    @NonNull
    public PAYLOAD_TYPE getPayload() {
      return payload;
    }

    @Override
    @NonNull
    public JWSObject getJWS() {
      return jws;
    }

    @NonNull
    @Override
    public String getSelfSignedVerifyingKeyID() throws JWSVerificationException {
      final List<JWK> matchingJWKs = getMatchingJWKFrom(getPayload().getPublicJWKSetOfSubject());
      if (matchingJWKs.isEmpty()) {
        throw new JWSVerificationException("JWS' header '" + jws.getHeader() + "' references NO JWK from its self-signed payload.");
      }

      return matchingJWKs.stream()
              .map(Impl::createVerifierWith)
              .filter(Objects::nonNull)
              .filter(this::jwsVerifiedSelfSignedBy)
              .findFirst()
              .map(JWKAndVerifier::getJWK)
              .map(JWK::getKeyID)
              .orElseThrow(() -> new JWSVerificationException("NO key from JWS payload's JWK-set verifies the JWS as self-signed."));
    }

    @NonNull
    @Override
    public String getVerifyingKeyIDFrom(@Nullable final JWKSet jwkSet) throws JWSVerificationException {
      final List<JWK> matchingJWKs = getMatchingJWKFrom(jwkSet);
      if (matchingJWKs.isEmpty()) {
        throw new JWSVerificationException("JWS' header '" + jws.getHeader() + "' references NO entry from provided JWK-set " + jwkSet + ".");
      }

      return matchingJWKs.stream()
              .map(Impl::createVerifierWith)
              .filter(Objects::nonNull)
              .filter(this::jwsVerifiedBy)
              .findFirst()
              .map(JWKAndVerifier::getJWK)
              .map(JWK::getKeyID)
              .orElseThrow(() -> new JWSVerificationException("Verification of JWS NOT successful with ANY key from provided JWK-set " + jwkSet + "."));
    }

    private List<JWK> getMatchingJWKFrom(@Nullable final JWKSet jwkSet) {
      return new JWKSelector(forJWSHeader(jws.getHeader()))
              .select(jwkSet);
    }

    private boolean jwsVerifiedBy(@NonNull final JWKAndVerifier jwkAndVerifier) {
      try {
        final boolean success = jws.verify(jwkAndVerifier.getVerifier());
        if (LOG.isDebugEnabled()) {
          LOG.debug("{} verification of '{}' with {}", (success ? "Successful" : "FAILED"), jws.serialize(), jwkAndVerifier.getJWK().toJSONString());
        }
        return success;
      } catch (final JOSEException e) {
        LOG.error("Internal error: Verification of JWS '{}' failed", jws, e);
        return false;
      }
    }

    private boolean jwsVerifiedSelfSignedBy(@NonNull final JWKAndVerifier jwkAndVerifier) {
      try {
        final boolean success = jws.verify(jwkAndVerifier.getVerifier());
        if (LOG.isDebugEnabled()) {
          LOG.debug("{} verification of self-signed '{}'", (success ? "Successful" : "FAILED"), jws.serialize());
        }
        return success;
      } catch (final JOSEException e) {
        LOG.error("Internal error: Verification of self-signed JWS '{}' failed", jws, e);
        return false;
      }
    }

    @Nullable
    public static JWKAndVerifier createVerifierWith(@Nullable final JWK jwk) {
      if (jwk == null) {
        return null;
      }

      final KeyType typeOfJWK = jwk.getKeyType();
      try {
        if (RSA.equals(typeOfJWK)) {
          return with(new RSASSAVerifier(jwk.toRSAKey()), jwk);
        }

        if (EC.equals(typeOfJWK)) {
          return with(new ECDSAVerifier(jwk.toECKey()), jwk);
        }

        LOG.error("Found NO JWS verifier matching JWK type '{}'", typeOfJWK);
      } catch (final JOSEException e) {
        LOG.error("Internal error: Creating JWS verifier with JWK '{}' failed", jwk.toJSONString(), e);
      }
      return null;
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o)
        return true;
      if (o == null || getClass() != o.getClass())
        return false;

      final Impl<?> of = (Impl<?>) o;

      if (!jws.equals(of.jws))
        return false;
      return payload.equals(of.payload);
    }

    @Override
    public int hashCode() {
      int result = jws.hashCode();
      result = 31 * result + payload.hashCode();
      return result;
    }

    @Override
    public String toString() {
      return "{" + payload +
              ", Plain JWS='" + jws.serialize() +
              "'}";
    }

  }

  @Immutable
  final class JWKAndVerifier {

    private final JWK jwk;
    private final JWSVerifier verifier;

    private JWKAndVerifier(@NonNull final JWSVerifier verifier, @NonNull final JWK jwk) {
      this.verifier = requireNonNull(verifier, "Missing verifier");
      this.jwk = requireNonNull(jwk, "Missing JWK");
    }

    @NonNull
    public static JWKAndVerifier with(@NonNull final JWSVerifier verifier, @NonNull final JWK jwk) {
      return new JWKAndVerifier(verifier, jwk);
    }

    @NonNull
    public JWK getJWK() {
      return jwk;
    }

    @NonNull
    public JWSVerifier getVerifier() {
      return verifier;
    }

  }

}
