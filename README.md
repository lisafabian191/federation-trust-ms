# Federation Trust Services

This project represents an entry point for [OpenID based Federation Trust](https://openid.net/specs/openid-connect-federation-1_0.html) service implementation skills.
It covers the following Open Source (OS) java reference implementations which can be re-used for
setting up customized trust verification of any OpenID based provider easily e.g. done by relying parties (RP).

* [Federation Trust Business-Lib](business)\
  A centralized core library used for trust verification process implementation. This lib get used by the following references.

* [Federation Trust Microservice](ms/README.md)\
  A simple web-service application that offers verification via http GET endpoint e.g. used by http clients (curl etc). 

* [Federation Trust User-Interface (Add-on)](ui/README.md)\
  A simple web-interface that offers visualized verification via browser based http requests.

## Rollout on DENIC infrastructure

See [corresponding DENIC ID project](https://gitlab.com/denic-id/federation-trust-ms).