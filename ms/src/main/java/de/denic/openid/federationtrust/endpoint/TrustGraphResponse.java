/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.endpoint;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.nimbusds.jose.jwk.JWKSet;
import de.denic.openid.federationtrust.business.EntityVertex;
import de.denic.openid.federationtrust.business.FedEntityStmtEdge;
import de.denic.openid.federationtrust.business.FederationTrustService;
import de.denic.openid.federationtrust.business.TrustGraph;
import de.denic.openid.federationtrust.common.Entity;
import io.swagger.v3.oas.annotations.media.Schema;
import net.minidev.json.JSONObject;
import org.jgrapht.GraphPath;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;
import static de.denic.openid.federationtrust.endpoint.TrustGraphResponse.JSONKeys.*;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@JsonPropertyOrder({ENTITY, CUSTOM_TRUST_ANCHORS, MAX_PATH_LENGTH, TRUST_ANCHOR_REACHED, CHAIN_VERIFIED, VERIFIED_JWK_SET, END_OF_VALIDITY, STARTED, FINISHED, DURATION, REACHED_TRUST_ANCHORS, CHAINS_OF_TRUST, NODES, EDGES})
@Immutable
public final class TrustGraphResponse {

  private static final String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";

  private final TrustGraph trustGraph;

  public TrustGraphResponse(@NonNull final TrustGraph trustGraph) {
    this.trustGraph = requireNonNull(trustGraph, "Missing trust graph data");
  }

  @Schema(name = ENTITY,
          description = "Requested Entity.",
          required = true)
  @NonNull
  @JsonGetter(ENTITY)
  public String getRequestedEntity() {
    return trustGraph.getLeaf().getPlainValue();
  }

  @Schema(name = CUSTOM_TRUST_ANCHORS,
          description = "Applied custom Trust Anchors.")
  @NonNull
  @JsonGetter(CUSTOM_TRUST_ANCHORS)
  public Collection<TrustAnchorResponse> getCustomTrustAnchors() {
    return TrustAnchorResponse.of(trustGraph.getCustomTrustAnchors());
  }

  @Schema(name = MAX_PATH_LENGTH,
          description = "Applied initial max path length (custom of default value).",
          minimum = "0",
          maximum = "" + FederationTrustService.ALLOWED_MAX_OF_MAX_PATH_LENGTH,
          required = true)
  @JsonGetter(MAX_PATH_LENGTH)
  public int getMaxPathLength() {
    return trustGraph.getMaxPathLength();
  }

  @Schema(name = TRUST_ANCHOR_REACHED,
          description = "Whether a (generic or custom) Trust Anchor has been reached.",
          required = true)
  @JsonGetter(TRUST_ANCHOR_REACHED)
  public boolean isTrustAnchorReached() {
    return trustGraph.isTrustAnchorReached();
  }

  @Schema(name = CHAIN_VERIFIED,
          description = "Whether Trust Chain reached some Trust Anchor AND could be verified successfully.",
          required = true)
  @JsonGetter(CHAIN_VERIFIED)
  // HINT: Name of method has to begin with "is", otherwise field will NOT appear in OpenAPI doc!
  public boolean isVerificationSucceeded() {
    return trustGraph.verificationHasSucceeded();
  }

  @Schema(name = REACHED_TRUST_ANCHORS,
          description = "Collection of reached (generic or custom) Trust Anchors.",
          required = true)
  @JsonGetter(REACHED_TRUST_ANCHORS)
  @NonNull
  public Set<Entity> getReachedTrustAnchors() {
    return getChainsOfTrust().stream()
            .filter(chain -> !chain.isEmpty())
            .map(chain -> chain.get(chain.size() - 1))
            .collect(toSet());
  }

  @Schema(name = VERIFIED_JWK_SET,
          description = "Requested Entity's verified JWK set.",
          format = "JSON according to RFC7517 (https://tools.ietf.org/html/rfc7517#section-5).")
  @Nullable
  @JsonGetter(VERIFIED_JWK_SET)
  public JSONObject getJWKSetOfLeaf() {
    final JWKSet jwkSetOfLeaf = trustGraph.getJWKSetOfLeaf();
    return (jwkSetOfLeaf == null ? null : jwkSetOfLeaf.toJSONObject());
  }

  @Schema(name = STARTED,
          description = "Timestamp starting construction of Trust Chain by remote operations to follow, formatted according to RFC3339.",
          format = TIMESTAMP_FORMAT,
          required = true)
  @NonNull
  @JsonGetter(STARTED)
  @JsonFormat(shape = STRING, pattern = TIMESTAMP_FORMAT)
  public ZonedDateTime getStarted() {
    return trustGraph.getStarted();
  }

  @Schema(name = FINISHED,
          description = "Timestamp when construction of Trust Chain by remote operations has been accomplished, formatted according to RFC3339.",
          format = TIMESTAMP_FORMAT,
          required = true)
  @NonNull
  @JsonGetter(FINISHED)
  @JsonFormat(shape = STRING, pattern = TIMESTAMP_FORMAT)
  public ZonedDateTime getFinished() {
    return trustGraph.getFinished();
  }

  @Schema(name = DURATION,
          description = "Duration of Trust Chain construction via remote operations, formatted according to ISO8601 (https://en.wikipedia.org/wiki/ISO_8601#Durations).",
          format = "PTnHnMnS",
          required = true)
  @NonNull
  @JsonGetter(DURATION)
  public String getDuration() {
    return trustGraph.getDuration().toString();
  }

  @Schema(name = END_OF_VALIDITY,
          description = "Timestamp the current Trust Chain ends its validity, formatted according to RFC3339.",
          format = TIMESTAMP_FORMAT)
  @Nullable
  @JsonGetter(END_OF_VALIDITY)
  @JsonFormat(shape = STRING, pattern = TIMESTAMP_FORMAT)
  public ZonedDateTime getEndOfValidity() {
    return trustGraph.getValidityInterval().getEnd().orElse(null);
  }

  @Schema(name = NODES,
          description = "Detailed info regarding nodes/vertices of Trust Graph.",
          required = true)
  @NonNull
  @JsonGetter(NODES)
  public List<VertexResponse> getNodes() {
    return VertexResponse.of(trustGraph.getGraph().vertexSet());
  }

  @Schema(name = EDGES,
          description = "Detailed info regarding edges of Trust Graph.")
  @NonNull
  @JsonGetter(EDGES)
  public List<EdgeResponse> getEdges() {
    return EdgeResponse.of(trustGraph.getGraph());
  }

  @Schema(name = CHAINS_OF_TRUST,
          implementation = Entity.class,
          description = "Array of arrays containing the edges/vertices making up valid Chain of Trust.",
          example = "[[\"nodeA1\",\"nodeA2\"],[\"nodeB\"]]")
  @NonNull
  @JsonGetter(CHAINS_OF_TRUST)
  public List<List<Entity>> getChainsOfTrust() {
    final List<GraphPath<EntityVertex, FedEntityStmtEdge>> validPathsToTrustAnchors = trustGraph.getValidPathsToTrustAnchors();
    return validPathsToTrustAnchors.stream()
            .map(path -> path.getVertexList()
                    .stream()
                    .map(EntityVertex::getEntity)
                    .collect(toList()))
            .collect(toList());
  }

  static final class JSONKeys {

    static final String ENTITY = "entity";
    static final String STARTED = "started";
    static final String FINISHED = "finished";
    static final String DURATION = "duration";
    static final String CUSTOM_TRUST_ANCHORS = "custom-trust-anchors";
    static final String NODES = "nodes";
    static final String VERIFIED_JWK_SET = "verified-jwk-set";
    static final String TRUST_ANCHOR_REACHED = "trust-anchor-reached";
    static final String END_OF_VALIDITY = "end-of-validity";
    static final String MAX_PATH_LENGTH = "max-path-length";
    static final String CHAIN_VERIFIED = "trust-chain-verified";
    static final String EDGES = "edges";
    static final String CHAINS_OF_TRUST = "chains-of-trust";
    static final String REACHED_TRUST_ANCHORS = "reached-trust-anchors";

  }

}
