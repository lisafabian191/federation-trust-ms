/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.endpoint;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.denic.openid.federationtrust.business.EntityVertex;
import de.denic.openid.federationtrust.common.JWSOf;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfig;
import io.swagger.v3.oas.annotations.media.Schema;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static de.denic.openid.federationtrust.endpoint.VertexResponse.JSONKeys.*;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;
import static java.util.stream.Collectors.toList;

@JsonPropertyOrder({ENTITY, IS_TRUST_ANCHOR, MAX_PATH_LENGTH, CURRENT_PATH_LENGTH, ISSUES, MESSAGES})
@Immutable
public final class VertexResponse {

  private static final Collection<EntityVertex> NO_ENTITY_VERTICES = emptyList();

  private final EntityVertex entityVertex;

  public VertexResponse(@NonNull final EntityVertex entityVertex) {
    this.entityVertex = requireNonNull(entityVertex, "Missing entity");
  }

  @Schema(name = ENTITY,
          description = "Entity.",
          required = true)
  @NonNull
  @JsonGetter(ENTITY)
  public String getEntityValue() {
    return entityVertex.getEntity().getPlainValue();
  }

  @Schema(name = MAX_PATH_LENGTH,
          description = "Max path length of this Entity.")
  @JsonGetter(MAX_PATH_LENGTH)
  @Nullable
  public Integer getMaxPathLengthOfEntity() {
    return entityVertex.getJWSOfFederationEntityConfig()
            .map(JWSOf::getPayload)
            .flatMap(FederationEntityConfig::getMaxPathLength)
            .orElse(null);
  }

  @Schema(name = CURRENT_PATH_LENGTH,
          description = "Current length of path ending at this Entity.")
  @JsonGetter(CURRENT_PATH_LENGTH)
  @Nullable
  public Integer getCurrentPathLength() {
    return entityVertex.getCurrentPathLength()
            .orElse(null);
  }

  @Schema(name = ISSUES,
          description = "Human readable severe issues of Entity.")
  @NonNull
  @JsonGetter(ISSUES)
  public List<String> getIssues() {
    return entityVertex.getIssues();
  }

  @Schema(name = MESSAGES,
          description = "Human readable further messages regarding Entity.")
  @NonNull
  @JsonGetter(MESSAGES)
  public List<String> getMessages() {
    return entityVertex.getMessages();
  }

  @Schema(name = IS_TRUST_ANCHOR,
          description = "If this entity is considered custom or generic Trust Anchor.")
  @Nullable
  @JsonGetter(IS_TRUST_ANCHOR)
  public Boolean isTrustAnchor() {
    return entityVertex.isTrustAnchor() ? TRUE : null;
  }

  @NonNull
  public static List<VertexResponse> of(final Collection<? extends EntityVertex> entityVertices) {
    return requireNonNullElse(entityVertices, NO_ENTITY_VERTICES).stream()
            .filter(Objects::nonNull)
            .map(VertexResponse::new)
            .collect(toList());
  }

  static final class JSONKeys {

    static final String ENTITY = "entity";
    static final String MAX_PATH_LENGTH = "max-path-length";
    static final String CURRENT_PATH_LENGTH = "current-path-length";
    static final String ISSUES = "issues";
    static final String MESSAGES = "messages";
    static final String IS_TRUST_ANCHOR = "is-trust-anchor";

  }

}
