/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.endpoint;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.nimbusds.jose.jwk.JWKSet;
import de.denic.openid.federationtrust.common.TrustAnchor;
import io.swagger.v3.oas.annotations.media.Schema;
import net.minidev.json.JSONObject;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import static de.denic.openid.federationtrust.common.TrustAnchor.NO_TRUST_ANCHORS;
import static de.denic.openid.federationtrust.endpoint.TrustAnchorResponse.JSONKeys.ENTITY;
import static de.denic.openid.federationtrust.endpoint.TrustAnchorResponse.JSONKeys.JWK_SET;
import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;
import static java.util.stream.Collectors.toList;

@JsonPropertyOrder({ENTITY, JWK_SET})
@Immutable
public final class TrustAnchorResponse {

  private final TrustAnchor trustAnchor;

  public TrustAnchorResponse(@NonNull final TrustAnchor trustAnchor) {
    this.trustAnchor = requireNonNull(trustAnchor, "Missing Trust Anchor");
  }

  @Schema(name = ENTITY,
          description = "Trust Anchor's Entity",
          required = true)
  @NonNull
  @JsonGetter(ENTITY)
  public String getEntityValue() {
    return trustAnchor.getEntity().getPlainValue();
  }

  @Schema(name = JWK_SET,
          description = "Applied Trust Anchor's custom JWK set",
          format = "JSON according to RFC7517 (https://tools.ietf.org/html/rfc7517#section-5).")
  @NonNull
  @JsonGetter(JWK_SET)
  public Optional<JSONObject> getJwkSetAsJSON() {
    return trustAnchor.getJwkSet().map(JWKSet::toJSONObject);
  }

  @NonNull
  public static Collection<TrustAnchorResponse> of(@Nullable final Collection<TrustAnchor> trustAnchors) {
    return requireNonNullElse(trustAnchors, NO_TRUST_ANCHORS).stream()
            .filter(Objects::nonNull)
            .map(TrustAnchorResponse::new)
            .collect(toList());
  }

  static final class JSONKeys {

    static final String ENTITY = "entity";
    static final String JWK_SET = "jwk-set";

  }

}
