/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.endpoint;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import de.denic.openid.federationtrust.business.TrustChain;
import io.swagger.v3.oas.annotations.media.Schema;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Collection;
import java.util.Objects;

import static de.denic.openid.federationtrust.endpoint.NodeResponse.JSONKeys.*;
import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.Objects.requireNonNullElse;
import static java.util.stream.Collectors.toList;

@JsonPropertyOrder({ENTITY, DESCRIPTION, MAX_PATH_LENGTH, CURRENT_PATH_LENGTH, AUTHORITY_HINTS})
@Immutable
public final class NodeResponse {

  private static final Collection<TrustChain.Node> NO_NODES = emptyList();

  private final TrustChain.Node node;

  public NodeResponse(@NonNull final TrustChain.Node trustChainNode) {
    this.node = requireNonNull(trustChainNode, "Missing node");
  }

  @Schema(name = ENTITY,
          description = "Entity.",
          required = true)
  @NonNull
  @JsonGetter(ENTITY)
  public String getEntityValue() {
    return node.getEntity().getPlainValue();
  }

  @Schema(name = AUTHORITY_HINTS,
          description = "Authority Hints reported by current Entity.")
  @JsonGetter(AUTHORITY_HINTS)
  @NonNull
  public Collection<NodeResponse> getAuthorityHints() {
    return NodeResponse.of(node.getNodesToFollow());
  }

  @Schema(name = MAX_PATH_LENGTH,
          description = "Max path length of this Entity.")
  @JsonGetter(MAX_PATH_LENGTH)
  @Nullable
  public Integer getMaxPathLengthOfEntity() {
    return node.getMaxPathLengthOfEntity();
  }

  @Schema(name = CURRENT_PATH_LENGTH,
          description = "Current length of path ending at this Entity.",
          required = true)
  @JsonGetter(CURRENT_PATH_LENGTH)
  @Nullable
  public Integer getCurrentPathLength() {
    return node.getCurrentPathLength();
  }

  @Schema(name = DESCRIPTION,
          description = "Human readable description of Entity or issues.",
          required = true)
  @NonNull
  @JsonGetter(DESCRIPTION)
  public String getDescription() {
    return node.getDescription();
  }

  @Nullable
  public static NodeResponse of(@Nullable final TrustChain.Node trustChainNode) {
    return (trustChainNode == null ? null : new NodeResponse(trustChainNode));
  }

  @NonNull
  private static Collection<NodeResponse> of(final Collection<? extends TrustChain.Node> trustChainNodes) {
    return requireNonNullElse(trustChainNodes, NO_NODES).stream()
            .map(NodeResponse::of)
            .filter(Objects::nonNull)
            .collect(toList());
  }

  static final class JSONKeys {

    static final String ENTITY = "entity";
    static final String DESCRIPTION = "description";
    static final String AUTHORITY_HINTS = "authority-hints";
    static final String MAX_PATH_LENGTH = "max-path-length";
    static final String CURRENT_PATH_LENGTH = "current-path-length";

  }

}
