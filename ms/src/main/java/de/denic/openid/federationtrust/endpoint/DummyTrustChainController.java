/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.endpoint;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.gen.ECKeyGenerator;
import de.denic.openid.federationtrust.business.FederationEntityStmt;
import de.denic.openid.federationtrust.common.Entity;
import de.denic.openid.federationtrust.common.JWSOf;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfig;
import de.denic.openid.federationtrust.wellknown.FederationEntityConfigClient;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.http.server.util.HttpHostResolver;
import io.micronaut.http.uri.UriBuilder;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.swagger.v3.oas.annotations.Hidden;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.net.URI;
import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Function;

import static de.denic.openid.federationtrust.wellknown.FederationEntityConfig.Impl.NO_AUTHORITY_HINTS;
import static io.micronaut.core.util.StringUtils.EMPTY_STRING;
import static io.micronaut.http.HttpStatus.NOT_FOUND;
import static io.micronaut.http.HttpStatus.OK;
import static java.time.ZonedDateTime.now;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;

@ThreadSafe
@Controller(DummyTrustChainController.CONTROLLER_PATH)
public final class DummyTrustChainController {

  public static final String CONTROLLER_PATH = "/dummy";

  private static final Logger LOG = LoggerFactory.getLogger(DummyTrustChainController.class);
  private static final String APPLICATION_JOSE = "application/jose";
  private static final String LEAF_SUFFIX = "leaf";
  private static final String AUTH_HINT_1_SUFFIX = "auth-hint-1";
  private static final String AUTH_HINT_2_SUFFIX = "auth-hint-2";
  private static final String AUTH_HINT_1_1_SUFFIX = "auth-hint-1-1";
  private static final String AUTH_HINT_1_2_SUFFIX = "auth-hint-1-2";
  private static final String AUTH_HINT_1_X_SUFFIX = "auth-hint-1-X";
  private static final String AUTH_HINT_1_1_1_SUFFIX = "auth-hint-1-1-1";
  private static final String AUTH_HINT_1_1_2_SUFFIX = "auth-hint-1-1-2";
  private static final String TRUST_ANCHOR_PATH_SUFFIX = "trust-anchor";
  private static final List<String> ALL_SUFFIXES = List.of(LEAF_SUFFIX, AUTH_HINT_1_SUFFIX, AUTH_HINT_2_SUFFIX,
          AUTH_HINT_1_1_SUFFIX, AUTH_HINT_1_2_SUFFIX, AUTH_HINT_1_X_SUFFIX,
          AUTH_HINT_1_1_1_SUFFIX, AUTH_HINT_1_1_2_SUFFIX,
          TRUST_ANCHOR_PATH_SUFFIX);
  private static final JsonNode NO_CONSTRAINTS_JSON_NODE = null;
  private static final JsonNode NO_META_DATA_JSON_NODE = null;
  private static final JsonNodeFactory JSON_NODE_FACTORY = JsonNodeFactory.instance;
  private static final Map<String, Set<String>> MAPPING_SUFFIXES_TO_ISSUERS_TO_SUBJECTS = new HashMap<>();
  private static final Entity TEST_SHOP_ENTITY = Entity.ofHost("rp.test.denic.de");
  private static final JWK TEST_SHOP_PUBLIC_JWK;

  static {
    MAPPING_SUFFIXES_TO_ISSUERS_TO_SUBJECTS.put(AUTH_HINT_1_1_2_SUFFIX, Set.of(AUTH_HINT_1_1_SUFFIX));
    MAPPING_SUFFIXES_TO_ISSUERS_TO_SUBJECTS.put(AUTH_HINT_1_1_1_SUFFIX, Set.of(AUTH_HINT_1_1_SUFFIX));
    MAPPING_SUFFIXES_TO_ISSUERS_TO_SUBJECTS.put(AUTH_HINT_1_1_SUFFIX, Set.of(AUTH_HINT_1_SUFFIX));
    MAPPING_SUFFIXES_TO_ISSUERS_TO_SUBJECTS.put(AUTH_HINT_1_2_SUFFIX, Set.of(AUTH_HINT_1_SUFFIX, AUTH_HINT_2_SUFFIX));
    MAPPING_SUFFIXES_TO_ISSUERS_TO_SUBJECTS.put(AUTH_HINT_1_X_SUFFIX, Set.of(AUTH_HINT_2_SUFFIX));
    MAPPING_SUFFIXES_TO_ISSUERS_TO_SUBJECTS.put(AUTH_HINT_1_SUFFIX, Set.of(LEAF_SUFFIX));
    MAPPING_SUFFIXES_TO_ISSUERS_TO_SUBJECTS.put(AUTH_HINT_2_SUFFIX, Set.of(LEAF_SUFFIX));
    try {
      TEST_SHOP_PUBLIC_JWK = JWK.parse("{" +
              "\"kty\": \"EC\"," +
              "\"use\": \"sig\"," +
              "\"crv\": \"P-256\"," +
              "\"kid\": \"DENIC-ID-Test-Shop\"," +
              "\"x\": \"yoWKh2ygjSKJ2Np4paXJv69m1AA2yrT-UESg4ae5spk\"," +
              "\"y\": \"C4IzWzg2WU8mn5WhGECeLVAMQLHwfSxcLUNCuQKr5q0\"" +
              "}");
    } catch (final ParseException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  private static final long IRRELEVANT_EPOCH_SECONDS = 0L;
  private static final FederationEntityConfig.Impl FEDERATION_ENTITY_CONFIG_OF_TEST_SHOP = new FederationEntityConfig.Impl(
          TEST_SHOP_ENTITY.asHttpsURI(),
          TEST_SHOP_ENTITY.asHttpsURI(),
          IRRELEVANT_EPOCH_SECONDS,
          IRRELEVANT_EPOCH_SECONDS,
          new JWKSet(TEST_SHOP_PUBLIC_JWK),
          NO_META_DATA_JSON_NODE,
          NO_CONSTRAINTS_JSON_NODE,
          NO_AUTHORITY_HINTS);

  private final HttpHostResolver httpHostResolver;
  private final FederationEntityConfigClient federationEntityConfigClient;
  private volatile Map<Entity, JWSOf<FederationEntityConfig>> entitiesMappedToFederationEntityConfig;

  public DummyTrustChainController(@NonNull final FederationEntityConfigClient federationEntityConfigClient,
                                   @NonNull final HttpHostResolver httpHostResolver) {
    this.httpHostResolver = requireNonNull(httpHostResolver, "Missing HTTP host resolver component");
    this.federationEntityConfigClient = requireNonNull(federationEntityConfigClient, "Missing Federation Entity Config Client component");
  }

  @Hidden
  @Get(uri = "/{entityPathSuffix}/.well-known/openid-federation", produces = APPLICATION_JOSE)
  public HttpResponse<String> wellKnownOpenIDFederation(@NonNull @PathVariable("entityPathSuffix") final String entityPathSuffix,
                                                        @NonNull final HttpRequest<?> httpRequest) {
    final Entity entity = prepareEntityFrom(entityPathSuffix, httpRequest);
    LOG.info("Well-known OpenID Federation queried for '{}'", entity);
    final JWSOf<FederationEntityConfig> jwsOfFederationEntityConfig;
    if (entitiesMappedToFederationEntityConfig == null) {
      final URI thisHostsURI = URI.create(httpHostResolver.resolve(httpRequest));
      entitiesMappedToFederationEntityConfig = prepareImmutableFederationEntityConfigsBasedOn(thisHostsURI);
    }
    jwsOfFederationEntityConfig = entitiesMappedToFederationEntityConfig.get(entity);
    final MutableHttpResponse<FederationEntityConfig> response = HttpResponse.status(jwsOfFederationEntityConfig == null ? NOT_FOUND : OK);
    return response.body((jwsOfFederationEntityConfig == null ? null : jwsOfFederationEntityConfig.getJWS().serialize()));
  }

  @Hidden
  @ExecuteOn(TaskExecutors.IO)
  @Get(uri = "/{entityPathSuffix}/federation-api-endpoint", produces = APPLICATION_JOSE)
  public HttpResponse<String> federationAPIEndpoint(@NonNull @PathVariable("entityPathSuffix") final String entityPathSuffix,
                                                    @NonNull @QueryValue("iss") final URI issuerURI,
                                                    @NonNull @QueryValue("sub") final URI subjectURI,
                                                    @NonNull final HttpRequest<?> httpRequest) {
    final Entity issuer = Entity.ofURI(issuerURI);
    final Entity subject = Entity.ofURI(subjectURI);
    LOG.info("Federation API endpoint queried with issuer '{}' and subject '{}'", issuer, subject);
    final URI resolvedHostURI = URI.create(httpHostResolver.resolve(httpRequest));
    if (entitiesMappedToFederationEntityConfig == null) {
      entitiesMappedToFederationEntityConfig = prepareImmutableFederationEntityConfigsBasedOn(resolvedHostURI);
    }
    final JWSOf<FederationEntityConfig> jwsOfFedEntityConfigOfIssuer = entitiesMappedToFederationEntityConfig.get(issuer);
    final FederationEntityConfig federationEntityConfigOfSubject;
    if (TRUST_ANCHOR_PATH_SUFFIX.equals(entityPathSuffix)) {
      federationEntityConfigOfSubject = trustAnchorPreparesJWSOfFederationEntityConfigOf(subject, resolvedHostURI);
    } else {
      final JWSOf<FederationEntityConfig> jwsOfFedEntityConfigOfSubject = entitiesMappedToFederationEntityConfig.get(subject);
      federationEntityConfigOfSubject = (jwsOfFedEntityConfigOfSubject == null ? null : jwsOfFedEntityConfigOfSubject.getPayload());
    }
    final FederationEntityConfig federationEntityConfigOfIssuer = (jwsOfFedEntityConfigOfIssuer == null ? null : jwsOfFedEntityConfigOfIssuer.getPayload());
    final JWSOf<FederationEntityStmt> jwsOfFederationEntityStmt = prepareJWSOfFederationEntityStmtRegarding(
            federationEntityConfigOfIssuer, federationEntityConfigOfSubject);
    final MutableHttpResponse<FederationEntityConfig> response = HttpResponse.status(jwsOfFederationEntityStmt == null ? NOT_FOUND : OK);
    return response.body((jwsOfFederationEntityStmt == null ? null : jwsOfFederationEntityStmt.getJWS().serialize()));
  }

  @Nullable
  private FederationEntityConfig trustAnchorPreparesJWSOfFederationEntityConfigOf(@NonNull final Entity subject,
                                                                                  @NonNull final URI resolvedHostURI) {
    if (ofSameParentDomain(resolvedHostURI, subject.asHttpsURI())) {
      return queryForFederationEntityConfigOf(subject).map(JWSOf::getPayload).orElse(null);
    }

    if (TEST_SHOP_ENTITY.equals(subject)) {
      return FEDERATION_ENTITY_CONFIG_OF_TEST_SHOP;
    }

    return null;
  }

  public static boolean ofSameParentDomain(@Nullable final URI uri1, @Nullable final URI uri2) {
    if (uri1 == null || uri2 == null) {
      return false;
    }

    final boolean result = (parentDomainOf(uri1).equals(parentDomainOf(uri2)));
    LOG.info("URIs '{}' and '{}' from {} parent domain(s)", uri1, uri2, (result ? "SAME" : "DIFFERENT"));
    return result;
  }

  @NonNull
  private static String parentDomainOf(@NonNull final URI uri) {
    final String host = requireNonNull(uri, "Missing URI").getHost();
    final int indexOfFirstDot = host.indexOf('.');
    return (indexOfFirstDot < 0 ? EMPTY_STRING : host.substring(indexOfFirstDot + 1));
  }

  @Nullable
  private JWSOf<FederationEntityStmt> prepareJWSOfFederationEntityStmtRegarding(@Nullable final FederationEntityConfig federationEntityConfigOfIssuer,
                                                                                @Nullable final FederationEntityConfig federationEntityConfigOfSubject) {
    if (federationEntityConfigOfIssuer == null || federationEntityConfigOfSubject == null) {
      return null;
    }

    final FederationEntityStmt result = prepareFederationEntityStmtRegarding(federationEntityConfigOfIssuer, federationEntityConfigOfSubject);
    return (result == null ? null : federationEntityConfigOfIssuer.sign(result));
  }

  @Nullable
  private FederationEntityStmt prepareFederationEntityStmtRegarding(@NonNull final FederationEntityConfig federationEntityConfigOfIssuer,
                                                                    @NonNull final FederationEntityConfig federationEntityConfigOfSubject) {
    final Entity issuer = federationEntityConfigOfIssuer.getIssuer();
    final Entity subject = federationEntityConfigOfSubject.getIssuer();
    final String suffixOfIssuer = getSuffixOf(issuer);
    final String suffixOfSubject = getSuffixOf(subject);
    if (!TRUST_ANCHOR_PATH_SUFFIX.equals(suffixOfIssuer)) {
      final Set<String> suffixesOfSupportedSubjects = MAPPING_SUFFIXES_TO_ISSUERS_TO_SUBJECTS.get(suffixOfIssuer);
      if (suffixesOfSupportedSubjects == null || !suffixesOfSupportedSubjects.contains(suffixOfSubject)) {
        LOG.info("Subject '{}' not supported by Issuer '{}'", subject, issuer);
        return null;
      }
    }

    final Long issuedAt;
    final Long expiresAt;
    if (AUTH_HINT_1_1_SUFFIX.equals(suffixOfIssuer) && AUTH_HINT_1_1_2_SUFFIX.equals(suffixOfSubject)) {
      issuedAt = epochSecondsOneWeekAgo();
      expiresAt = epochSecondsOneMinuteAgo();
    } else if (TEST_SHOP_ENTITY.equals(federationEntityConfigOfSubject.getIssuer())) {
      issuedAt = epochSecondsOfNow();
      expiresAt = epochSecondsThreeYearsAhead();
    } else {
      issuedAt = epochSecondsOneMinuteAgo();
      expiresAt = epochSecondsOneDayAhead();
    }
    return new FederationEntityStmt.Impl(issuer.asHttpsURI(),
            subject.asHttpsURI(),
            issuedAt,
            expiresAt,
            federationEntityConfigOfSubject.getPublicJWKSetOfSubject());
  }

  @NonNull
  private Map<Entity, JWSOf<FederationEntityConfig>> prepareImmutableFederationEntityConfigsBasedOn(@NonNull final URI resolvedHostURI) {
    final Map<Entity, JWSOf<FederationEntityConfig>> result = new HashMap<>();
    ALL_SUFFIXES.stream()
            .map(entitySuffix -> this.prepareEntityFrom(entitySuffix, resolvedHostURI))
            .forEach(entity -> result.put(entity, prepareJWSOfFederationEntityConfigOf(entity, resolvedHostURI)));
    return unmodifiableMap(result);
  }

  private JWSOf<FederationEntityConfig> prepareJWSOfFederationEntityConfigOf(@NonNull final Entity entity,
                                                                             @NonNull final URI resolvedHostURI) {
    final ECKey jwkOfEntity;
    try {
      jwkOfEntity = new ECKeyGenerator(Curve.P_256)
              .keyID(getSuffixOf(entity) + "-key")
              .generate();
    } catch (final JOSEException e) {
      throw new RuntimeException("Internal error: Creating EC key failed", e);
    }

    final Collection<URI> authorityHints = getAuthHintsOf(entity, resolvedHostURI);
    final Long issuedAt;
    final Long expiresAt;
    if (entity.getPlainValue().endsWith(AUTH_HINT_1_X_SUFFIX)) {
      issuedAt = epochSecondsOneWeekAgo();
      expiresAt = epochSecondsOneMinuteAgo();
    } else {
      issuedAt = epochSecondsOneMinuteAgo();
      expiresAt = epochSecondsOneMonthAhead();
    }
    final JWKSet jwkSetOfEntity = new JWKSet(jwkOfEntity);
    final FederationEntityConfig federationConfigOfEntity = new FederationEntityConfig.Impl(entity.asHttpsURI(), entity.asHttpsURI(),
            issuedAt, expiresAt,
            jwkSetOfEntity,
            prepareFederationAPIEndpointMetaDataNodeOf(entity),
            NO_CONSTRAINTS_JSON_NODE,
            authorityHints);
    return JWSOf.signed(federationConfigOfEntity, jwkOfEntity);
  }

  private JsonNode prepareFederationAPIEndpointMetaDataNodeOf(@NonNull final Entity entity) {
    final ObjectNode metaDataNode = JSON_NODE_FACTORY.objectNode();
    metaDataNode.putObject("federation_entity")
            .put("federation_api_endpoint", UriBuilder.of(entity.asHttpsURI()).path("federation-api-endpoint").build().toASCIIString());
    return metaDataNode;
  }

  @NonNull
  private Entity prepareEntityFrom(@NonNull final String entitySuffix, @NonNull final HttpRequest<?> httpRequest) {
    final String resolvedHostURI = this.httpHostResolver.resolve(requireNonNull(httpRequest, "Missing HTTP request"));
    return prepareEntityFrom(entitySuffix, URI.create(resolvedHostURI));
  }

  @NonNull
  private Entity prepareEntityFrom(@NonNull final String entitySuffix, @NonNull final URI resolvedHostURI) {
    final URI entitiesURI = UriBuilder.of(resolvedHostURI)
            .path(CONTROLLER_PATH)
            .path(requireNonNull(entitySuffix, "Missing entity suffix"))
            .build();
    return Entity.ofURI(entitiesURI);
  }

  @NonNull
  private List<URI> getAuthHintsOf(@NonNull final Entity entity, @NonNull final URI resolvedHostURI) {
    final String entitySuffix = getSuffixOf(entity);
    final List<String> authHintSuffices;
    switch (entitySuffix) {
      case LEAF_SUFFIX:
        authHintSuffices = List.of("auth-hint-0", AUTH_HINT_1_SUFFIX, AUTH_HINT_2_SUFFIX);
        break;
      case AUTH_HINT_1_SUFFIX:
        authHintSuffices = List.of(AUTH_HINT_1_1_SUFFIX, AUTH_HINT_1_2_SUFFIX);
        break;
      case AUTH_HINT_2_SUFFIX:
        authHintSuffices = List.of(AUTH_HINT_1_2_SUFFIX, AUTH_HINT_1_X_SUFFIX);
        break;
      case AUTH_HINT_1_1_SUFFIX:
        authHintSuffices = List.of(AUTH_HINT_1_1_1_SUFFIX, AUTH_HINT_1_1_2_SUFFIX);
        break;
      default:
        authHintSuffices = emptyList();
    }
    return authHintSuffices.stream()
            .distinct()
            .map(authHint -> prepareEntityFrom(authHint, resolvedHostURI))
            .map(Entity::asHttpsURI)
            .collect(toList());
  }

  @NonNull
  private String getSuffixOf(@NonNull final Entity entity) {
    final String entityURIPath = requireNonNull(entity, "Missing entity").asHttpsURI().getPath();
    final int lastIndexOfSlash = entityURIPath.lastIndexOf('/');
    return entityURIPath.substring(lastIndexOfSlash + 1);
  }

  @NonNull
  private static Long epochSecondsThreeYearsAhead() {
    return epochSecondsOfNowWith(now -> now.plusYears(3L));
  }

  @NonNull
  private static Long epochSecondsOneMonthAhead() {
    return epochSecondsOfNowWith(now -> now.plusMonths(1L));
  }

  @NonNull
  private static Long epochSecondsOneDayAhead() {
    return epochSecondsOfNowWith(now -> now.plusDays(1L));
  }

  @NonNull
  private static Long epochSecondsOfNow() {
    return epochSecondsOfNowWith(identity());
  }

  @NonNull
  private static Long epochSecondsOneMinuteAgo() {
    return epochSecondsOfNowWith(now -> now.minusMinutes(1L));
  }

  @NonNull
  private static Long epochSecondsOneWeekAgo() {
    return epochSecondsOfNowWith(now -> now.minusWeeks(1L));
  }

  private static long epochSecondsOfNowWith(@NonNull final Function<ZonedDateTime, ZonedDateTime> modification) {
    return requireNonNull(modification, "Missing modification function")
            .apply(now())
            .toEpochSecond();
  }

  @NonNull
  private Optional<JWSOf<FederationEntityConfig>> queryForFederationEntityConfigOf(@Nullable final Entity entity) {
    if (entity == null) {
      return empty();
    }

    return federationEntityConfigClient.getInfoFrom(entity);
  }

}
