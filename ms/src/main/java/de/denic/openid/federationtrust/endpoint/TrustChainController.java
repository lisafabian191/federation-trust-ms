/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.endpoint;

import de.denic.openid.federationtrust.business.FederationTrustService;
import de.denic.openid.federationtrust.business.TrustChain;
import de.denic.openid.federationtrust.common.Entity;
import de.denic.openid.federationtrust.common.TrustAnchor;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.swagger.v3.oas.annotations.Hidden;
import org.slf4j.MDC;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.core.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.time.ZonedDateTime;

import static de.denic.openid.federationtrust.business.FederationTrustService.MDC_LEAF;
import static io.micronaut.http.HttpHeaders.CACHE_CONTROL;
import static io.micronaut.http.HttpHeaders.EXPIRES;
import static io.micronaut.http.HttpStatus.NOT_FOUND;
import static io.micronaut.http.HttpStatus.OK;
import static io.micronaut.http.MediaType.APPLICATION_JSON;
import static java.time.ZonedDateTime.now;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Objects.requireNonNull;

@Immutable
@ExecuteOn(TaskExecutors.IO)
@Controller("/trust/chain")
public final class TrustChainController {

  private final FederationTrustService federationTrustService;

  public TrustChainController(@NonNull final FederationTrustService federationTrustService) {
    this.federationTrustService = requireNonNull(federationTrustService, "Missing Federation Trust Service component");
  }

  @Hidden
  @Get(uri = "/{entity}{?trust-anchor,ta-jwkset,max-path-length}", produces = APPLICATION_JSON)
  public HttpResponse<TrustChainResponse> get(@NonNull @PathVariable("entity") final String host,
                                              @Nullable @QueryValue("trust-anchor") final String trustAnchorValue,
                                              @Nullable @QueryValue("ta-jwkset") final String trustAnchorJWKSetValue,
                                              @Nullable @QueryValue("max-path-length") final Integer maxPathLength) {
    final Entity entity = Entity.ofHost(host);
    MDC.put(MDC_LEAF, entity.getPlainValue());
    try {
      final TrustAnchor trustAnchor = TrustAnchor.fromEntityAndJWKSet(trustAnchorValue, trustAnchorJWKSetValue);
      final TrustChain trustChain = federationTrustService.seekForChainOf(entity)
              .withMaxPathLength(maxPathLength)
              .withCustomTrustAnchors(trustAnchor)
              .now();
      final boolean overallSuccess = trustChain.isTrustAnchorReached() && trustChain.immediateVerificationSucceeds();
      final ZonedDateTime endOfValidityInterval = trustChain.getValidityInterval().getEnd().orElse(now());
      final MutableHttpResponse<TrustChainResponse> response = HttpResponse.status(overallSuccess ? OK : NOT_FOUND);
      response.getHeaders()
              .add(EXPIRES, endOfValidityInterval)
              .set(CACHE_CONTROL, (overallSuccess ? "max-age=" + now().until(endOfValidityInterval, SECONDS)
                      : "no-cache"));
      return response.body(new TrustChainResponse(trustChain));
    } finally {
      MDC.clear();
    }
  }

}
