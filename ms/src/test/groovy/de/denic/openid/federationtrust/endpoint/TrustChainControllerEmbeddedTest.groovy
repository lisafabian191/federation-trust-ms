/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.endpoint

import com.nimbusds.jose.jwk.Curve
import com.nimbusds.jose.jwk.JWK
import com.nimbusds.jose.jwk.JWKSet
import com.nimbusds.jose.jwk.gen.ECKeyGenerator
import com.nimbusds.jose.util.Base64URL
import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.runtime.server.EmbeddedServer
import spock.lang.Shared
import spock.lang.Specification

import static com.nimbusds.jose.jwk.KeyUse.SIGNATURE
import static io.micronaut.http.HttpStatus.NOT_FOUND

class TrustChainControllerEmbeddedTest extends Specification {

  @Shared
  EmbeddedServer server = ApplicationContext.run(EmbeddedServer)
  @Shared
  HttpClient client = server.getApplicationContext()
          .createBean(HttpClient, server.getURL())

  def cleanupSpec() {
    client?.stop()
    server?.stop()
  }

  def 'Querying for host not run by an Identity Agent'() {
    when:
    client.toBlocking().retrieve(
            '/trust/chain/example.com')

    then:
    def responseException = thrown(HttpClientResponseException)
    responseException.status == NOT_FOUND
    responseException.response.body() ==~ /\{"entity":"example.com","max-path-length":4,"trust-anchor-reached":false,"trust-chain-verified":false,"started":"([0-9T\-:]+)Z","finished":"([0-9T\-:]+)Z","duration":"PT([0-9.]+)S","chain":\{"entity":"example.com","description":"NO Federation Entity Config available."}}/
  }

  def "Querying for host not run by an Identity Agent with 'trust-anchor' entity"() {
    when:
    client.toBlocking().retrieve(
            '/trust/chain/example.com?trust-anchor=trust-anchor.example&max-path-length=2')

    then:
    def responseException = thrown(HttpClientResponseException)
    responseException.status == NOT_FOUND
    responseException.response.body() ==~ /\{"entity":"example.com","trust-anchors":\[\{"entity":"trust-anchor\.example"}],"max-path-length":2,"trust-anchor-reached":false,"trust-chain-verified":false,"started":"([0-9T\-:]+)Z","finished":"([0-9T\-:]+)Z","duration":"PT([0-9.]+)S","chain":\{"entity":"example.com","description":"NO Federation Entity Config available."}}/
  }

  def "Querying for host not run by an Identity Agent with 'trust-anchor' entity and JWKSet"() {
    given:
    JWK jwk = new ECKeyGenerator(Curve.P_256)
            .keyUse(SIGNATURE)
            .keyID('testKeyID')
            .generate()
    JWKSet jwkSet = new JWKSet(jwk)
    def jwkSetJSON = jwkSet.toPublicJWKSet().toJSONObject().toString()
    def base64JWKSetJSON = Base64URL.encode(jwkSetJSON).toString()

    when:
    client.toBlocking().retrieve(
            "/trust/chain/example.com?trust-anchor=trust-anchor.example&ta-jwkset=${base64JWKSetJSON}")

    then:
    def responseException = thrown(HttpClientResponseException)
    responseException.status == NOT_FOUND
    def regExpOfJWKSetJSON = jwkSetJSON.replace('{', '\\{').replace('[', '\\[')
    responseException.response.body() ==~ /\{"entity":"example.com","trust-anchors":\[\{"entity":"trust-anchor\.example","jwk-set":${regExpOfJWKSetJSON}}],"max-path-length":4,"trust-anchor-reached":false,"trust-chain-verified":false,"started":"([0-9T\-:]+)Z","finished":"([0-9T\-:]+)Z","duration":"PT([0-9.]+)S","chain":\{"entity":"example.com","description":"NO Federation Entity Config available."}}/
  }

}
