/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.endpoint

import spock.lang.Specification

import static de.denic.openid.federationtrust.endpoint.DummyTrustChainController.ofSameParentDomain

class DummyTrustChainControllerTest extends Specification {

  def "OfSameParentDomain"(String uri1, String uri2, boolean result) {
    expect:
    ofSameParentDomain(URI.create(uri1), URI.create(uri2)) == result

    where:
    uri1                      | uri2                      | result
    'http://tld'              | 'http://tld'              | true
    'http://first.tld'        | 'http://other.tld'        | true
    'http://second.first.tld' | 'http://other.first.tld'  | true
    'http://first.tld'        | 'http://first.other'      | false
    'http://second.first.tld' | 'http://second.other.tld' | false
  }

}
