/*^
  ===========================================================================
  ID4me Federation Trust Services
  ===========================================================================
  Copyright (C) 2020-2021 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.openid.federationtrust.endpoint

import com.nimbusds.jose.JWSObject
import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.http.uri.UriBuilder
import io.micronaut.runtime.server.EmbeddedServer
import spock.lang.Shared
import spock.lang.Specification

import static io.micronaut.http.HttpStatus.NOT_FOUND

class DummyTrustChainControllerEmbeddedTest extends Specification {

  @Shared
  EmbeddedServer server = ApplicationContext.run(EmbeddedServer)
  @Shared
  HttpClient client = server.getApplicationContext()
          .createBean(HttpClient, server.getURL())

  def cleanupSpec() {
    client?.stop()
    server?.stop()
  }

  def 'Querying for Well-Known endpoint of leaf Entity'() {
    when:
    String response = client.toBlocking().retrieve(
            '/dummy/leaf/.well-known/openid-federation')

    then:
    def responseAsJWSObject = JWSObject.parse(response)
    responseAsJWSObject.getHeader().toString() == '{"kid":"leaf-key","alg":"ES256"}'
    def serverHttpsURI = UriBuilder.of(server.URI).scheme('https').build()
    String jwsPaylod = responseAsJWSObject.getPayload().toString()
    println "Received JWS payload: ${jwsPaylod}"
    jwsPaylod.startsWith('{"iss":"' + serverHttpsURI + '/dummy/leaf","sub":"' + serverHttpsURI + '/dummy/leaf"')
    jwsPaylod =~ "\"authority_hints\":\\[\"${serverHttpsURI}/dummy/auth-hint-(\\d)\",\"${serverHttpsURI}/dummy/auth-hint-(\\d)\",\"${serverHttpsURI}/dummy/auth-hint-(\\d)\"\\]"
    jwsPaylod =~ "\"metadata\":\\{\"federation_entity\":\\{\"federation_api_endpoint\":\"${serverHttpsURI}/dummy/leaf/federation-api-endpoint\"\\}\\}"
  }

  def 'Querying for Well-Known endpoint of unknown Entity'() {
    when:
    client.toBlocking().retrieve(
            '/dummy/unknown/.well-known/openid-federation')

    then:
    def responseException = thrown(HttpClientResponseException)
    responseException.status == NOT_FOUND
  }

  def 'Querying Federation API endpoint of Auth Endpoint for leaf Entity'() {
    given:
    def serverHttpsURI = UriBuilder.of(server.URI).scheme('https').build()

    when:
    String response = client.toBlocking().retrieve(
            "/dummy/auth-hint-1/federation-api-endpoint?iss=${serverHttpsURI}/dummy/auth-hint-1&sub=${serverHttpsURI}/dummy/leaf")

    then:
    def responseAsJWSObject = JWSObject.parse(response)
    responseAsJWSObject.getHeader().toString() == '{"kid":"auth-hint-1-key","alg":"ES256"}'
    String jwsPaylod = responseAsJWSObject.getPayload().toString()
    println "Received JWS payload: ${jwsPaylod}"
    jwsPaylod.startsWith('{"iss":"' + serverHttpsURI + '/dummy/auth-hint-1","sub":"' + serverHttpsURI + '/dummy/leaf"')
    !jwsPaylod.contains("authority_hints")
    !jwsPaylod.contains("metadata")
  }

}
