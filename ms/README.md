# Federation Trust Micro Service

## Building

Project's artifacts are built with [Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html).
You do **not** have to install this tool by hand, it bootstraps itself if necessary.

### Executable [`shadow` (or `fat`/`uber`)](https://github.com/johnrengelman/shadow) JAR file

To create an executable JAR file issue:

```bash
$ ./gradlew clean assemble
```

You will find the result in folder `./ms/build/libs`, zipped/tarred distributions in `./ms/build/distributions/`.
To also execute tests replace task `assemble` by `build`.

### [Docker](https://www.docker.com/) image

Project uses [Google's JIB Gradle plugin](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin) to
create Docker artifacts **without** requiring a local Docker daemon. That's why you won't find a
`Dockerfile` with the project.  

Creating a `tar` file containing the image, and upload the resulting artifact to your local image repository works like this:

```bash
$ ./gradlew jibBuildTar
[...]

$ docker load --input ms/build/jib-image.tar
[...]

$ docker image list | grep federation
registry.gitlab.com/id4me/federation-trust-ms    latest    a8565cd7d7e0    12 seconds ago    211MB
```

More details on [JIB's documentation](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin).


## Running

General information: Application logs the HTTP endpoint it listens on to console:

```
[...]
[main] INFO  - Startup completed in 1109ms. Server Running: http://localhost:8080
[...]
```

### Local Java Runtime Environment (JRE)

At least JRE version 11 is required!

```bash
$ java -jar ./ms/build/libs/federationtrust-ms-0.1-all.jar
```

### Docker container

Launching a container from the image built previously:

```bash
$ docker run --publish 8080:8080 registry.gitlab.com/id4me/federation-trust-ms
[...]
```

Now you can reach the container from your host computer:

```bash
$ curl --silent localhost:8080/health | jq .
{
  "status": "UP"
}
```

### Local with [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

```bash
$ cd ms

/ms$ heroku local
[...]
```


## Deployment

### To [Heroku](https://www.heroku.com/) platform

**Prerequisite** is an account on Heroku platform.
[Login to your account](https://devcenter.heroku.com/articles/heroku-cli#getting-started) if not already done.

#### Via [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
  
First of all you need [Heroku's Java CLI plugin](https://github.com/heroku/plugin-java):
```bash
$ heroku plugins:install java
Installing plugin java... installed v3.1.1
```

Now create the Heroku app (which exists only local, not on Heroku's Git repo or something like that), here with the app name `federation-trust-ms` in Heroku's EU region, using project's Heroku [Procfile](ms/Procfile):
```bash
$ heroku create --no-remote --manifest --region eu federation-trust-ms
Creating ⬢ federation-trust-ms... done, region is eu
https://federation-trust-ms.herokuapp.com/ | https://git.heroku.com/federation-trust-ms.git
```

As you can see, application domain `federation-trust-ms.herokuapp.com` now is blocked for your attempts, so you have to choose another app name, sorry :blush:

Final step: Deployment of the app's artifact, here our executable JAR file:

```bash
$ cd ms

/ms$ heroku deploy:jar --app federation-trust-ms --jdk 14 build/libs/federationtrust-ms-0.1-all.jar
Uploading federationtrust-ms-0.1-all.jar
-----> Packaging application...
       - app: federation-trust-ms
       - including: build/libs/federationtrust-ms-0.1-all.jar
-----> Creating build...
       - file: slug.tgz
       - size: 14MB
-----> Uploading build...
       - success
-----> Deploying...
remote:
remote: -----> heroku-deploy app detected
remote: -----> Installing JDK 14... done
remote: -----> Discovering process types
remote:        Procfile declares types -> web
remote:
remote: -----> Compressing...
remote:        Done: 80.2M
remote: -----> Launching...
remote:        Released v3
remote:        https://federation-trust-ms.herokuapp.com/ deployed to Heroku
remote:
-----> Done
```

Querying Heroku apps now gives:
```bash
$ heroku apps
=== [USER_ACCOUNT] Apps
federation-trust-ms (eu)
[...]
```

Getting more info about this app:
```bash
$ heroku apps:info --app federation-trust-ms
=== federation-trust-ms
Auto Cert Mgmt: false
Dynos:          web: 1
Git URL:        https://git.heroku.com/federation-trust-ms.git
Owner:          [USER_ACCOUNT]
Region:         eu
Repo Size:      0 B
Slug Size:      80 MB
Stack:          heroku-18
Web URL:        https://federation-trust-ms.herokuapp.com/
```

#### Via [Gradle's Heroku plugin](https://github.com/heroku/heroku-gradle)

Simply run:

```bash
$ ./gradlew deployHeroku
[...]
```


### To a [Cloud Foundry](https://www.cloudfoundry.org/) platform

**Prerequisite** is an account on target Cloud Foundry platform.

#### Via [Cloud Foundry CLI](https://docs.cloudfoundry.org/cf-cli/install-go-cli.html)

If not already done, first of all login to your Cloud Foundry provider's infrastructure like this:

```bash
$ cf login -u [YOUR_USER]
```

You will be quoted for your credential/password, and maybe also for
* the API endpoint of your provider (which you can also provide via  
  `-a [API_ENDPOINT_URL]` flag)
* one of the organisations you have set up on your provider (also possible via  
  `-o [YOUR_ORGANISATION]` flag)
* one of the spaces you have set up for your organisation (also possible via  
  `-s [SPACE_OF_ORGANISATION]` flag) 

Make a local copy of project's file `cf-vars.yml.example` ...

```bash
$ cp cf-vars.yml.example cf-vars.yml
$ chmod 0600 cf-vars.yml
```

... and configure your copy `cf-vars.yml` appropriately, especially key `CF_ROUTE_DOMAIN` to hold the domain your provider offers. 
Now you should be able to push the application to your Cloud Foundry provider's infrastructure:

```bash
$ cd ms

/ms$ cf push --vars-file ../cf-vars.yml
[...]

$ cf apps | grep federation
federation-trust-ms    started    1/1    384M    512M    federation-trust-ms.[CF_ROUTE_DOMAIN]
```

#### Via [Gradle's Cloud Foundry Plugin](https://github.com/vmwarepivotallabs/ya-cf-app-gradle-plugin)

Make a local copy of project's file `gradle-local.properties.example` ...

```bash
$ cp gradle-local.properties.example gradle-local.properties
$ chmod 0600 gradle-local.properties
```

... and configure your copy `gradle-local.properties` appropriately.
Here you have to configure a lot more compared to the aforementioned way via Cloud Foundry CLI,
cause the plugin does **not** require this CLI, especially it does not re-use its user login/session.

Now you should be able to push the application to your configured Cloud Foundry provider:

```bash
$ ./gradlew cf-push
[...]
```

Additionally, your get further Gradle tasks to manage your Cloud Foundry application/infrastructure.
Here a shortened list (further details on [plugin's README](https://github.com/vmwarepivotallabs/ya-cf-app-gradle-plugin/blob/master/README.adoc)):

```bash
$ $ ./gradlew tasks | grep cf-
  cf-create-services - Create a set of services
  cf-delete-app - Delete an application from Cloud Foundry
  [...]
  cf-push - Pushes an Application to Cloud Foundry
  [...]
  cf-start-app - Start an Application
  cf-stop-app - Stop an Application
  cf-unmap-route - Remove an existing route for an application
```


### With [helm (Kubernetes package manager)](https://helm.sh/)

This project offers a public helm chart repo for all released versions under the [projects gitlab webpage](https://id4me.gitlab.io/federation-trust-ms/index.yaml).

First of all you have add the chart repo to your local helm config (only once):
```bash
$ helm repo add id4me-charts https://id4me.gitlab.io/federation-trust-ms
"id4me-charts" has been added to your repositories
```

To get the latest updates and changes from the repo issue the following:
```bash
$ helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "id4me-charts" chart repository
Update Complete. ⎈ Happy Helming!⎈
```

Check the available versions with:
```bash
$ helm search repo id4me-charts/federation-trust-ms --versions
NAME                            	CHART VERSION 	APP VERSION   	DESCRIPTION                                       
id4me-charts/federation-trust-ms	1.0.0+07fd7d4a	1.0.0-07fd7d4a	A simplified service for OpenID based federatio...
id4me-charts/federation-trust-ms	1.0.0+f41818ef	1.0.0-f41818ef	A simplified service for OpenID based federatio...
id4me-charts/federation-trust-ms	1.0.0+77b6b15f	1.0.0-77b6b15f	A simplified service for OpenID based federatio...
id4me-charts/federation-trust-ms	1.0.0+6a3bed2c	1.0.0-6a3bed2c	A simplified service for OpenID based federatio...
id4me-charts/federation-trust-ms	1.0.0+965adc79	1.0.0-965adc79	A simplified service for OpenID based federatio...
id4me-charts/federation-trust-ms	1.0.0+51e2c0ab	1.0.0-51e2c0ab	A simplified service for OpenID based federatio...
```

In order to deploy a corresponding version (parameter &lt;CHART VERSION&gt;) directly from the repo into a K8s clusters namespace (parameter &lt;NAMESPACE&gt;) use the following command:
```bash
$ helm upgrade \
   --install federation-trust-ms id4me-charts/federation-trust-ms \
   --version <CHART VERSION> \
   --namespace=<NAMESPACE> \
   --wait \
   --timeout=600s

Release "federation-trust-ms" has been upgraded. Happy Helming!
STATUS: deployed
```
For passing user specified values instead of default add --values &lt;VALUES YAML FILE&gt; to the command above.  

## Documentation of used libs

### Feature kubernetes documentation

- [Micronaut Kubernetes Distributed Configuration documentation](https://micronaut-projects.github.io/micronaut-kubernetes/latest/guide/index.html)

### Feature management documentation

- [Micronaut Micronaut Management documentation](https://docs.micronaut.io/latest/guide/index.html#management)

### Feature http-client documentation

- [Micronaut Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)
