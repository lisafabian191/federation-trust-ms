FROM openjdk:11-jre-slim

RUN echo "Updating data of package repositories ..." && \
    echo "Installing package graphviz....." && \
    apt-get update && \
    apt-get install graphviz --yes